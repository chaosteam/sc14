{-# OPTIONS_GHC -Wall -fno-warn-dodgy-imports #-}
module Main (main) where

import Data.IORef
import Data.List
import Data.Version
import Distribution.Package
import Distribution.PackageDescription
import Distribution.Simple
import Distribution.Simple.BuildPaths
import Distribution.Simple.LocalBuildInfo hiding (componentName)
import Distribution.Simple.Setup
import Distribution.Simple.Utils
import Distribution.Verbosity
import System.Directory
import System.FilePath ( (</>) )

main :: IO ()
main = defaultMainWithHooks simpleUserHooks
  { buildHook = \pkg lbi hooks flags -> do
     generateBuildModule (fromFlag (buildVerbosity flags)) pkg lbi flags
     buildHook simpleUserHooks pkg lbi hooks flags
  }

--  Very ad-hoc implementation of difference lists
singletonDL :: a -> [a] -> [a]
singletonDL = (:)

emptyDL :: [a] -> [a]
emptyDL = id

appendDL :: ([a] -> [a]) -> ([a] -> [a]) -> [a] -> [a]
appendDL x y = x . y

componentName :: Component -> ComponentName
componentName =
  foldComponent (const CLibName)
                (CExeName . exeName)
                (CTestName . testName)
                (CBenchName . benchmarkName)

generateBuildModule :: Verbosity -> PackageDescription -> LocalBuildInfo -> BuildFlags -> IO ()
generateBuildModule verbosity pkg lbi flags = do
  let dir = autogenModulesDir lbi
  createDirectoryIfMissingVerbose verbosity True dir
  depsVar <- newIORef emptyDL
  withLibLBI pkg lbi $ \lib liblbi ->
    modifyIORef depsVar $ appendDL . singletonDL $ depsEntry (libBuildInfo lib) liblbi
  withExeLBI pkg lbi $ \exe exelbi ->
    modifyIORef depsVar $ appendDL . singletonDL $ depsEntry (buildInfo exe) exelbi
  deps <- fmap (nub . ($ [])) $ readIORef depsVar

  withComponents pkg $ \comp -> do
    srcDirs <- mapM canonicalizePath $ hsSourceDirs $ componentBuildInfo comp
    distDir <- canonicalizePath $ fromFlagOrDefault defaultDistPref $ buildDistPref flags

    let compName CLibName = let (PackageName n) = pkgName $ package pkg in n
        compName (CTestName n) = n
        compName (CExeName n) = n
        compName (CBenchName n) = n

    rewriteFile ((dir </>) $ map fixchar $ "Build_" ++ compName (componentName comp) ++ ".hs") $ unlines
      [ "module Build_" ++ map fixchar (compName (componentName comp)) ++ " where"
      , "getDistDir :: FilePath"
      , "getDistDir = " ++ show distDir
      , "getSrcDirs :: [FilePath]"
      , "getSrcDirs = " ++ show srcDirs
      , "deps :: [([FilePath], [String])]"
      , "deps = " ++ show deps
      ]

  where
    formatdeps = map (formatone . snd)
    formatone p = case packageName p of
      PackageName n -> n ++ "-" ++ showVersion (packageVersion p)
    depsEntry targetbi targetlbi = (hsSourceDirs targetbi, formatdeps $ componentPackageDeps targetlbi)
    fixchar '-' = '_'
    fixchar c = c

withComponents :: PackageDescription -> (Component -> IO ()) -> IO ()
withComponents pkg f = mapM_ f components where
  components = concat
    [ [ CLib lib | Just lib <- [library pkg] ]
    , map CExe $ executables pkg
    , map CTest $ testSuites pkg
    , map CBench $ benchmarks pkg
    ]
