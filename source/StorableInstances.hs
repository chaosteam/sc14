{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}

module StorableInstances where

import Control.Lens
import Data.Bits
import Data.Functor
import Data.List
import Data.Word
import Foreign.Ptr
import Foreign.Storable

import qualified Data.IntMap as IM

import Game.Game
import Game.Player
import Game.Position
import Game.Stone

padAlign :: Int -> Int -> Int
padAlign a s
  | r == 0     = s
  | otherwise = s + a - r
  where r = s `rem` a

-- Note: This instance assumes that
-- inBounds p == True for Positions p that are
-- used with it
instance Storable Position where
  sizeOf    _ = 1
  alignment _ = 1

  peek ptr = do
    (x :: Word8) <- peek $ castPtr ptr
    return $ Position (fromIntegral $ 0xF .&. x `shiftR` 4) (fromIntegral $ 0xF .&. x)

  poke ptr (Position x y) =
    poke (castPtr ptr) (fromIntegral $ x `shiftL` 4 .|. y :: Word8)


instance Storable Stone where
  sizeOf    _ = 1
  alignment _ = 1

  peek ptr = do
    (x :: Word8) <- peek $ castPtr ptr
    return $ toEnum $ fromIntegral x

  poke ptr st =
    poke (castPtr ptr) (fromIntegral $ fromEnum st :: Word8)


instance Storable StoneCollection where
  sizeOf    _ = 13
  alignment _ = 1

  peek ptr = do
    (len :: Word8) <- peek $ castPtr ptr
    view stoneCollection . IM.fromDistinctAscList <$> go len (plusPtr ptr 1)

    where go 0   _= return []
          go !n !p = do
            (st :: Word8) <- peek $ castPtr p
            (c  :: Word8) <- peek $ plusPtr p $ sizeOf st
            ((fromIntegral st, fromIntegral c):) <$> go (n - 1) (plusPtr p $ sizeOf st + 1)

  poke ptr sc = do
    poke (castPtr ptr) (fromIntegral $ IM.size m :: Word8)
    go (IM.toList m) $ plusPtr ptr 1

    where m = review stoneCollection sc

          go []              _ = return ()
          go ((st, c):rest) !p = do
            poke p (fromIntegral st :: Word8)
            poke (plusPtr p 1) (fromIntegral c :: Word8)
            go rest $ plusPtr p 2


instance Storable Move where
  sizeOf    _ = 2 + 6 * (sizeOf (undefined :: Stone) + sizeOf (undefined :: Position))
  alignment _ = alignment (undefined :: Stone) -- = alignment (undefined :: Position)

  peek ptr = do
    (tag :: Word8) <- peek $ castPtr ptr
    (len :: Word8) <- peek $ plusPtr ptr 1
    let ptr' = plusPtr ptr 2
    case tag of
      1 -> ExchangeMove <$> peekExchange len ptr'
      2 -> LayMove      <$> peekLay      len ptr'
      _ -> error "Storable Move: invalid peek"

    where peekExchange 0   _ = return []
          peekExchange !n !p = do
            (st :: Stone) <- peek p
            (st:) <$> peekExchange (n - 1) (plusPtr p $ sizeOf st)

          peekLay 0   _ = return []
          peekLay !n !p = do
            (st  :: Stone)    <- peek p
            (pos :: Position) <- peek $ plusPtr p $ sizeOf st
            ((st, pos):) <$> peekLay (n - 1) (plusPtr p $ sizeOf st + sizeOf pos)

  poke ptr (ExchangeMove sts) = do
    flip poke (1                 :: Word8) $ castPtr ptr
    flip poke (genericLength sts :: Word8) $ plusPtr ptr 1
    go sts $ plusPtr ptr 2

    where go []         _ = return ()
          go (st:rest) !p = do
            poke p st
            go rest $ plusPtr p $ sizeOf st

  poke ptr (LayMove lays) = do
    flip poke (2                  :: Word8) $ castPtr ptr
    flip poke (genericLength lays :: Word8) $ plusPtr ptr 1
    go lays $ plusPtr ptr 2

    where go []               _ = return ()
          go ((st,pos):rest) !p = do
            poke p st
            let p' = plusPtr p $ sizeOf st
            poke p' pos
            go rest $ plusPtr p' $ sizeOf pos


instance Storable MaybeMove where
  sizeOf    _ = sizeOf (undefined :: Move)
  alignment _ = alignment (undefined :: Move)

  peek ptr = MaybeMove <$> do
    -- e is the constructor tag also used by the storable instance for Move
    (e :: Word8) <- peek (castPtr ptr)
    case e of
      -- The storable instance for Move doesn't use 0 as a constructor tag, so we can use it here
      0 -> return Nothing
      _ -> Just <$> peek (castPtr ptr)

  poke ptr (MaybeMove Nothing)  = poke (castPtr ptr) (0 :: Word8)
  poke ptr (MaybeMove (Just m)) = poke (castPtr ptr) m


instance Storable PlayerState where
  sizeOf    _ = padAlign (alignment (undefined :: Word64)) $ sizeOf (undefined :: Word64) + sizeOf (undefined :: StoneCollection)
  alignment _ = alignment (undefined :: Word64)

  peek ptr = do
    pts <- peek $ castPtr ptr
    stc <- peek $ plusPtr ptr $ sizeOf pts
    return $ PlayerState pts stc

  poke ptr (PlayerState pts stc) = do
    flip poke pts $ castPtr ptr
    flip poke stc $ plusPtr ptr $ sizeOf pts


instance Storable s => Storable (PlayerData s) where
  sizeOf    _ = 2 * padAlign (alignment (undefined :: s)) (sizeOf (undefined :: s))
  alignment _ = alignment (undefined :: s)

  peek ptr = do
    r <- peek $ castPtr ptr
    b <- peek $ plusPtr ptr $ sizeOf r
    return $ PlayerData r b

  poke ptr (PlayerData r b) = do
    flip poke r $ castPtr ptr
    flip poke b $ plusPtr ptr $ sizeOf r


instance Storable GameState where
  sizeOf    _ = padAlign (alignment (undefined :: PlayerData PlayerState)) (4 + 12 * sizeOf (undefined :: Stone)) + sizeOf (undefined :: PlayerData PlayerState)
  alignment _ = alignment (undefined :: PlayerData PlayerState)

  peek ptr = do
    (nsc :: Word8) <- peek $ castPtr ptr
    (nc  :: Word8) <- peek $ plusPtr ptr 1
    (stc :: Word8) <- peek $ plusPtr ptr 2
    (t   :: Word8) <- peek $ plusPtr ptr 3
    ns <- peekNextStones nsc $ plusPtr ptr 4
    ps <- peek $ ptr `plusPtr` 4 `plusPtr` (sizeOf (undefined :: Stone) * fromIntegral nsc) `alignPtr` alignment (undefined :: PlayerData PlayerState)
    return $ GameState ns ps (toEnum $ fromIntegral nc) (fromIntegral stc) (fromIntegral t)

    where peekNextStones 0   _ = return []
          peekNextStones !n !p = do
            st <- peek p
            (st:) <$> peekNextStones (n - 1) (p `plusPtr` sizeOf st)

  poke ptr (GameState ns ps nc stc t) = do
    flip poke (genericLength ns           :: Word8) $ castPtr ptr
    flip poke (fromIntegral $ fromEnum nc :: Word8) $ plusPtr ptr 1
    flip poke (fromIntegral stc           :: Word8) $ plusPtr ptr 2
    flip poke (fromIntegral t             :: Word8) $ plusPtr ptr 3
    pokeNextStones ns $ plusPtr ptr 4
    flip poke ps $ ptr `plusPtr` 4 `plusPtr` (sizeOf (undefined :: Stone) * length ns) `alignPtr` alignment (undefined :: PlayerData PlayerState)

    where pokeNextStones []        _ = return ()
          pokeNextStones (st:sts) !p = do
            poke p st
            pokeNextStones sts $ plusPtr p $ sizeOf st
