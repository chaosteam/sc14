{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RankNTypes #-}

module Game.Player
  ( PlayerColor(..), oppositePlayerColor
  , PlayerState(PlayerState), points, stones
  , PlayerData(PlayerData), red, blue, playerWithColor
  , Points
  ) where

import Control.DeepSeq.Generics
import Control.Lens
import Data.Int
import GHC.Generics
import Instances.TH.Lift()
import Language.Haskell.TH.Lift

import Game.Stone

type Points = Int16

-- | The color that a player may have.
data PlayerColor = PlayerRed | PlayerBlue deriving (Eq, Ord, Show, Enum, Generic)
deriveLift ''PlayerColor
instance NFData PlayerColor where rnf = genericRnf

-- | The opposite player color is the color of the enemy.
oppositePlayerColor :: PlayerColor -> PlayerColor
oppositePlayerColor PlayerRed = PlayerBlue
oppositePlayerColor PlayerBlue = PlayerRed

-- | The state of the player. This contains the points and the stones of the player.
data PlayerState = PlayerState
  { _points :: !Points
  , _stones :: !StoneCollection
  } deriving (Show, Eq, Ord, Generic)
makeLenses ''PlayerState
deriveLift ''PlayerState
instance NFData PlayerState where rnf = genericRnf

-- | Store some state for each player.
data PlayerData s = PlayerData
  { _red        :: !s
  , _blue       :: !s
  } deriving (Show, Eq, Ord, Functor, Generic)
makeLenses ''PlayerData
deriveLift ''PlayerData
instance NFData s => NFData (PlayerData s) where rnf = genericRnf

playerWithColor :: PlayerColor -> Lens' (PlayerData s) s
playerWithColor PlayerRed  = red
playerWithColor PlayerBlue = blue
{-# INLINE playerWithColor #-}
