{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
module Game.Game
  ( GameState(..), nextStones, players, nextColor, stonesLeft, turn, nextPlayerData, currentPlayerData
  , GameResult(..), reason, winner
  , GameConfiguration(..), enemyColor, self, enemy, startPlayer, nextPlayer, currentPlayer
  ) where

import Control.DeepSeq.Generics
import Control.Lens
import GHC.Generics (Generic)
import Language.Haskell.TH.Lift

import qualified Data.Text as T

import Game.Player
import Game.Stone

-- | The result of the game, as reported by the server.
data GameResult = GameResult
  { _winner :: Maybe PlayerColor
  , _reason :: T.Text
  } deriving Show
makeLenses ''GameResult

-- | Configuration of the game (our color, start player color, etc)
-- This data stays constant for the whole game.
data GameConfiguration = GameConfiguration
  { selfColor  :: PlayerColor
  , startColor :: PlayerColor
  }

-- | Get the color of the enemy.
enemyColor :: GameConfiguration -> PlayerColor
enemyColor = oppositePlayerColor . selfColor

-- | Get a lens into PlayerData for our own state.
self :: GameConfiguration -> Lens' (PlayerData s) s
self = playerWithColor . selfColor
{-# INLINE self #-}

-- | Get a lens into PlayerData for the opponents state.
enemy :: GameConfiguration -> Lens' (PlayerData s) s
enemy = playerWithColor . enemyColor
{-# INLINE enemy #-}

-- | Get a lens into PlayerData for the start player's state.
startPlayer :: GameConfiguration -> Lens' (PlayerData s) s
startPlayer = playerWithColor . startColor
{-# INLINE startPlayer #-}

-- | The current state of the game.
data GameState = GameState
  { _nextStones :: [Stone]
  , _players    :: PlayerData PlayerState
  , _nextColor  :: PlayerColor
  , _stonesLeft :: Int
  , _turn       :: Int
  } deriving (Show, Eq, Generic)
makeLenses ''GameState
deriveLift ''GameState
instance NFData GameState where rnf = genericRnf

-- | Get a lens into PlayerData for the next player's state.
nextPlayer :: GameState -> Lens' (PlayerData s) s
nextPlayer gs = playerWithColor (gs^.nextColor)
{-# INLINE nextPlayer #-}

-- | Like nextPlayer, but specialized for GameState.
nextPlayerData :: Lens' GameState PlayerState
nextPlayerData f gs = players (nextPlayer gs f) gs
{-# INLINE nextPlayerData #-}

-- | Get a lens into PlayerData for the current player's state.
currentPlayer :: GameState -> Lens' (PlayerData s) s
currentPlayer gs = playerWithColor (gs^.nextColor.to oppositePlayerColor)
{-# INLINE currentPlayer #-}

-- | Like currentPlayer, but specialized for GameState.
currentPlayerData :: Lens' GameState PlayerState
currentPlayerData f gs = players (currentPlayer gs f) gs
{-# INLINE currentPlayerData #-}
