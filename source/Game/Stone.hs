{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Game.Stone
  ( StoneColor(..), StoneSymbol(..)
  , Stone(..), color, symbol
  , allStoneColors
  , allStoneSymbols
  , allStones

  , StoneCollection(), stoneCollection
  , makeStoneCollection
  , stoneCount
  , stoneList
  , anyStone
  , putStone
  , popStone
  , takeStone
  , isComplete
  , refillStones

  , Move(..), _ExchangeMove, _LayMove
  , usedStones
  , takeMoveStones
  , isValidStartMove
  , defaultMove

  , MaybeMove(..)
  ) where

import Control.DeepSeq.Generics
import Control.Lens
import Data.Bits
import Data.IntMap (IntMap, toList, fromList)
import Data.List
import GHC.Generics (Generic)
import Instances.TH.Lift()
import Language.Haskell.TH.Lift

import qualified Data.Map as M

import Game.Position

--------------------------------------------------------------------------------
-- | The symbol of a stone.
data StoneSymbol = Acorn | Bell | Clubs | Diamond | Heart | Spades deriving (Show, Enum, Eq, Ord, Read, Generic)
deriveLift ''StoneSymbol
instance NFData StoneSymbol where rnf = genericRnf

allStoneSymbols :: [StoneSymbol]
allStoneSymbols = [Acorn .. Spades]

-- | The color of a stone.
data StoneColor = Blue | Green | Magenta | Orange | Violet | Yellow deriving (Show, Enum, Eq, Ord, Read, Generic)
deriveLift ''StoneColor
instance NFData StoneColor where rnf = genericRnf

allStoneColors :: [StoneColor]
allStoneColors = [Blue .. Yellow]

-- | A stone which has a symbol and a color.
data Stone = Stone
  { _symbol     :: !StoneSymbol
  , _color      :: !StoneColor
  } deriving (Eq, Ord, Read, Generic)
deriveLift ''Stone
makeLenses ''Stone
instance NFData Stone where rnf = genericRnf

instance Show Stone where
  showsPrec d (Stone s c) = showParen (d > 10) $ showString "Stone " . showsPrec 11 s . showString " " . showsPrec 11 c

{- Note [Enum instance for Stone]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
There are 6 different symbols and 6 different colors. This means
that there are 6 * 6 = 36 different stones. We assign an unique number
to each stone (0 <= n < 64). We use the 3 bits of n for the color
and the other 3 bits for the symbol. So for example, @Stone Clubs Red@
is represented as a 6 bit integer, where 3 bits are @fromEnum Red@ and the
other 3 bits are @fromEnum Clubs@. This works because fromEnum x < 2^3 for
all x, where x is a color or a symbol.
-}
instance Enum Stone where
  fromEnum (Stone s c) = fromEnum s `shiftL` 3 .|. fromEnum c
  toEnum i = Stone (toEnum s') (toEnum c')
    where s' = (i .&. 0x38) `shiftR` 3 -- 0x38 is 00111000 in binary.
          c' = i .&. 0x07 -- 0x07 is 00000111 in binary.

-- | A set of all possible stones.
allStones :: [Stone]
allStones = [ Stone s c | s <- allStoneSymbols, c <- allStoneColors ]

--------------------------------------------------------------------------------
-- | A collection of stones which can efficiently compute how many stones of a given kind
-- the collection contains.
newtype StoneCollection = StoneCollection (IntMap Int) deriving (Show, Eq, Ord, NFData)
makeIso ''StoneCollection
deriveLift ''StoneCollection

-- | Given a map of stones and their corresponding count, create a new stone collection.
makeStoneCollection :: M.Map Stone Int -> StoneCollection
makeStoneCollection = StoneCollection . fromList . map (over _1 fromEnum) . M.toList

-- | Count how many stones of the given kind are contained in the stone collection.
stoneCount :: Stone -> StoneCollection -> Int
stoneCount s (StoneCollection m) = m ^. at (fromEnum s) . non 0

-- | Get a list of all stones in the collection.
stoneList :: StoneCollection -> [Stone]
stoneList (StoneCollection m) = toList m >>= \(i, n) -> replicate n $ toEnum i

-- | Get a single stone from the stone set.
anyStone :: StoneCollection -> Stone
anyStone = head . stoneList

-- | Insert a stone into the collection.
putStone :: StoneCollection -> Stone -> StoneCollection
putStone m s = m & from stoneCollection . at (fromEnum s) . non 0 +~ 1

-- | Remove a stone from the collection.
popStone :: StoneCollection -> Stone -> StoneCollection
popStone m s = m & from stoneCollection . at (fromEnum s) . non' (nearly 0 (<= 0)) -~ 1

-- | Remove a single stone from the stone collection, and replace it with the next stone
-- in the list. If the list is empty, the resulting stone collection will be incomplete.
takeStone :: Stone -> ([Stone], StoneCollection) -> ([Stone], StoneCollection)
takeStone s ([]  , stc) = ([], popStone stc s)
takeStone s (n:ns, stc) = (ns, popStone (putStone stc n) s)

-- | Refill a stone collection.
-- This will take replacement stones from the next stones for all missing stones.
-- It also takes the number of stones used by the move creating this situation
-- and updates the next stones accordingly.
refillStones :: Int -> [Stone] -> StoneCollection -> ([Stone], StoneCollection)
refillStones used nextstones collection@(StoneCollection m) = (nextstones', foldl' putStone collection fills)
  where (fills, nextstones') = splitAt remaining $ drop (used - remaining) nextstones
        remaining = 6 - sum (map snd $ toList m)

--------------------------------------------------------------------------------
-- | A move represents the action a player does in a single turn.
data Move = ExchangeMove [Stone] | LayMove [(Stone, Position)] deriving (Show, Read, Generic)
deriveLift ''Move
makePrisms ''Move
instance NFData Move where rnf = genericRnf

instance Eq Move where
  ExchangeMove as == ExchangeMove bs = sort as == sort bs
  LayMove as      == LayMove      bs = sort as == sort bs
  _               == _               = False

instance Ord Move where
  ExchangeMove as `compare` ExchangeMove bs = compare (sort as) (sort bs)
  ExchangeMove _  `compare` LayMove _       = LT
  LayMove as      `compare` LayMove bs      = compare (sort as) (sort bs)
  LayMove _       `compare` ExchangeMove _ = GT

-- | Get a list of all stones used by the move.
usedStones :: Move -> [Stone]
usedStones (ExchangeMove xs) = xs
usedStones (LayMove lays)    = map fst lays

-- | Remove all stones used by the given move and replace them by using the first
-- stones from the given list of next stones. Return the new stone list and collection.
-- If the move uses more stones that available in the next stones list, return Nothing.
takeMoveStones :: Move -> [Stone] -> StoneCollection -> ([Stone], StoneCollection)
takeMoveStones move ns stc = (ns', foldl' putStone (foldl' popStone stc sts) reps)
  where (reps, ns') = splitAt (length sts) ns
        sts = usedStones move

-- | Check whether the stone collection of a player is complete or if there
-- are missing any stones (the number of stones is not 6).
-- Returns True if the stone collection is complete, False otherwise.
isComplete :: StoneCollection -> Bool
isComplete (StoneCollection m) = sum (map snd $ toList m) == 6

-- | Check whether the move can be used as start move.
isValidStartMove :: Move -> Bool
isValidStartMove (ExchangeMove _) = True
isValidStartMove (LayMove l) | length l > 1 = True
                             | otherwise    = False

defaultMove :: StoneCollection -> Move
defaultMove = ExchangeMove . stoneList

newtype MaybeMove = MaybeMove (Maybe Move) deriving (Eq, Show)
