{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}
module Game.StoneSet
  ( StoneSet()
  , empty
  , null
  , singleton
  , insert
  , delete
  , member
  , intersection
  , difference
  , union
  , toList
  , fromList
  , size
  ) where

import Control.DeepSeq
import Control.Lens
import Data.Bits
import Data.Bits.Lens
import Data.List (foldl')
import Data.Semigroup
import Data.Word
import Prelude hiding (null, all)

import Game.Stone

import qualified Data.MemoStatic as Memo

{- Note [StoneSet]
~~~~~~~~~~~~~~~~~~
We can represent a set of stones with just 64 bits,
where the nth bit is set iff the stone with that fromEnum value is in the set.
-}

-- | This is like @Set Stone@, but more performant.
newtype StoneSet = StoneSet Word64 deriving (Eq, NFData, Ord)

instance Show StoneSet where
  show ss = "fromList " ++ show (toList ss)

instance Monoid StoneSet where
  mempty = empty
  mappend = union

instance Semigroup StoneSet where
  (<>) = union

stonemask :: Iso' StoneSet Word64
stonemask = iso ?? StoneSet $ \(StoneSet x) -> x
{-# INLINE stonemask #-}

stonebit :: Stone -> Lens' StoneSet Bool
stonebit s = stonemask . bitAt (fromEnum s)
{-# INLINE stonebit #-}

empty :: StoneSet
empty = StoneSet 0

null :: StoneSet -> Bool
null (StoneSet m) = m == 0

singleton :: Stone -> StoneSet
singleton s = StoneSet $ 1 `shiftL` fromEnum s

insert :: Stone -> StoneSet -> StoneSet
insert s = stonebit s .~ True
{-# INLINE insert #-}

delete :: Stone -> StoneSet -> StoneSet
delete s = stonebit s .~ False
{-# INLINE delete #-}

member :: Stone -> StoneSet -> Bool
member s (StoneSet m) = m .&. (1 `shiftL` fromEnum s) /= 0

intersection :: StoneSet -> StoneSet -> StoneSet
intersection (StoneSet m) (StoneSet m') = StoneSet $ m .&. m'

difference :: StoneSet -> StoneSet -> StoneSet
difference (StoneSet m) (StoneSet m') = StoneSet $ m .&. complement m'

union :: StoneSet -> StoneSet -> StoneSet
union (StoneSet m) (StoneSet m') = StoneSet $ m .|. m'

#define TO_LIST_N(f, n) \
  f :: Word64 -> [Stone]; \
  f = $(Memo.staticLit (map (`unsafeShiftL` n) [0 :: Word64 .. 255]) [| \x -> map (toEnum . (+ n)) $ filter (testBit (x `unsafeShiftR` n :: Word64)) [0..7] |])

toList :: StoneSet -> [Stone]
toList = \(StoneSet m) -> toList64 m where
  TO_LIST_N(go0, 0)
  TO_LIST_N(go1, 8)
  TO_LIST_N(go2, 16)
  TO_LIST_N(go3, 24)
  TO_LIST_N(go4, 32)
  TO_LIST_N(go5, 40)
  TO_LIST_N(go6, 48)
  TO_LIST_N(go7, 56)

  toList64 :: Word64 -> [Stone]
  toList64 w = go0 (w .&. 0xFF)
            ++ go1 (w .&. 0xFF00)
            ++ go2 (w .&. 0xFF0000)
            ++ go3 (w .&. 0xFF000000)
            ++ go4 (w .&. 0xFF00000000)
            ++ go5 (w .&. 0xFF0000000000)
            ++ go6 (w .&. 0xFF000000000000)
            ++ go7 (w .&. 0xFF00000000000000)

fromList :: [Stone] -> StoneSet
fromList = foldl' (flip insert) empty

size :: StoneSet -> Int
size = view $ stonemask . to popCount
