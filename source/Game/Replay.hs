{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TemplateHaskell #-}
module Game.Replay
  ( Replay(..)
  , GameSnapshot(..)
  , board
  , allGameStates
  ) where

import Data.List.NonEmpty (NonEmpty(), (<|))
import Language.Haskell.TH.Lift

import Game.Board
import Game.Game
import Game.Player
import Game.Position
import Game.Stone

-- | A game situation represents the state of the game at a single turn. It doesn't
-- contain data that stays constant for the whole game, such as the game configuration.
data GameSnapshot = GameSnapshot
  { move       :: Move                -- ^ The move that was made at this point
  , gameState  :: GameState           -- ^ The game state before the move was made
  , laidStones :: [(Stone, Position)] -- ^ The state of the board after the move was made.
  }
deriveLift ''GameSnapshot

-- | A replay describes an already-played game. It may be read from a replay file, which
-- is provided by the server.
data Replay = Replay
  { gameSnapshots :: [GameSnapshot]
  , gameResult     :: (GameState, GameResult)
  , playerNames    :: PlayerData String
  }

-- | Extract the board for a game situation, as it was after the move for the situation was made.
board :: GameSnapshot -> Board
board GameSnapshot{..} = applyMove (LayMove laidStones) newBoard

-- | Get all game states of the game. This includes the game state after the last turn, so this
-- list will have at least one, but at most 41 elements.
allGameStates :: Replay -> NonEmpty GameState
allGameStates (Replay{..}) = foldr ((<|) . gameState) (return $ fst gameResult) gameSnapshots
