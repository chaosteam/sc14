{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Game.Board
  ( SeriesType(..)
  , Series()
  , Field(), newField, free, blocked, inBoard, allPossibleStones, getSeriesLength, getHoledSeriesLength
  , Board(), newBoard, singleFieldBoard, isEmpty, lookupPosition, deletePosition, allFreeFields, displayBoard
  , updateDirection, updateAxis, insertStone, applyMove
  ) where

import Control.DeepSeq
import Control.DeepSeq.Generics
import Control.Lens
import Data.Int
import Data.List
import Data.Maybe
import Data.Monoid
import GHC.Generics (Generic)

import qualified Data.IntMap.Strict as M

import Game.Position
import Game.Stone

import qualified Data.MemoStatic as Memo
import qualified Game.StoneSet as SS

-- | A set of all stones with the given color.
stonesWithColor :: StoneColor -> SS.StoneSet
stonesWithColor = $(Memo.static ''StoneColor $ [| \c -> SS.fromList [ Stone s c | s <- allStoneSymbols ] |])

-- | A set of all stones with the given symbol.
stonesWithSymbol :: StoneSymbol -> SS.StoneSet
stonesWithSymbol = $(Memo.static ''StoneSymbol $ [| \s -> SS.fromList [ Stone s c | c <- allStoneColors ] |])

-- | This data type represents the different types of series.
--
-- If we have more than one stone, all stones must have either the same color or the same symbol.
-- If there is only one stone, then the type is ambiguous.
-- We also inculde a @Invalid@ constructor for the semigroup instance and a @NoSeries@ constructor for the monoid instance.
data SeriesType = SameColor StoneColor | SameSymbol StoneSymbol | Ambiguous StoneColor StoneSymbol | Invalid | NoSeries deriving (Show, Eq, Ord, Generic)
instance NFData SeriesType where rnf = genericRnf

-- | The monoid instance calculates the intersection of two series types.
--
-- It is useful to determine the possible stones at any given field. To do this, we need to intersect
-- the horizontal series with the vertical series.
instance Monoid SeriesType where
  mempty = NoSeries

  -- For simple series with more than one stone, only series
  -- that have the same common color/same common symbol can be composed.
  SameColor c    `mappend` SameColor c'     | c == c' = SameColor c
  SameSymbol s   `mappend` SameSymbol s'    | s == s' = SameSymbol s
  -- An ambiguous series can be composed with other series
  -- that either have the same color OR the same symbol
  SameColor c    `mappend` Ambiguous c' _   | c == c' = SameColor c
  Ambiguous c' _ `mappend` SameColor c      | c == c' = SameColor c
  SameSymbol s   `mappend` Ambiguous _ s'   | s == s' = SameSymbol s
  Ambiguous _ s' `mappend` SameSymbol s     | s == s' = SameSymbol s
  Ambiguous c s  `mappend` Ambiguous c' s'  | c == c' = SameColor  c
                                     | s == s' = SameSymbol s
  -- Composing a series with no other series just gives the series again
  NoSeries       `mappend` a                          = a
  a              `mappend` NoSeries                   = a
  -- Any other combination is invalid
  _              `mappend` _                          = Invalid

-- | A series of stones.
-- We store the type of the series, because we only need to determine that once and can cache it.
data Series = Series
  { _seriesStones :: {-# UNPACK #-} !SS.StoneSet
  , _seriesType :: !SeriesType
  } deriving (Show, Eq, Generic, Ord)
makeLenses ''Series
instance NFData Series where rnf = genericRnf

instance Monoid Series where
  mempty = Series mempty mempty
  mappend a b = a
              & seriesStones <>~ b^.seriesStones
              & seriesType   <>~ b^.seriesType

-- | Extend a series with a given stone by inserting that stone into the series.
--
-- Note: This won't check that the stone actually fits in the series.
extendSeries :: Stone -> Series -> Series
extendSeries st s = s <> Series (SS.singleton st) (Ambiguous (st^.color) (st^.symbol))

-- | A series of stones with a "hole" in it.
--
-- The hole represents a place where a stone can be laid.
-- It can be either at one end of the series (in that case, one of seriesBefore or seriesAfter will be empty)
-- or somewhere in the middle.
data HoledSeries = HoledSeries
  { -- | The series before the hole.
    _seriesBefore :: !Series

    -- | The series after the hole.
  , _seriesAfter  :: !Series
  } deriving (Show, Eq, Generic, Ord)
makeLenses ''HoledSeries
instance NFData HoledSeries where rnf = genericRnf

-- | Insert a stone into a holed series, filling the hole.
--
-- Note: It's not checked that the stone actually fits.
fillSeriesHole :: Stone -> HoledSeries -> Series
fillSeriesHole st s = extendSeries st $ s^.seriesBefore <> s^.seriesAfter

-- | Return all stones that fit in the hole of a HoledSeries.
matchingHoleStones :: HoledSeries -> SS.StoneSet
matchingHoleStones s = missingStones $ s^.seriesBefore.seriesStones <> s^.seriesAfter.seriesStones
  where stype :: SeriesType
        stype = s^.seriesBefore.seriesType <> s^.seriesAfter.seriesType

        missingStones :: SS.StoneSet -> SS.StoneSet
        missingStones sts = (`SS.difference` sts) $ case stype of
          Invalid        -> mempty
          SameColor c    -> stonesWithColor c
          SameSymbol sy  -> stonesWithSymbol sy
          Ambiguous c sy -> stonesWithSymbol sy `SS.union` stonesWithColor c
          NoSeries       -> SS.fromList allStones

--------------------------------------------------------------------------------
-- | A field contains information about a position on the board.
--
-- We store two @HoldedSeries@. One for each axis, vertical or horizontal.
data Field = Field
  { _possible   :: SS.StoneSet -- This is just for caching, and can be calculated from the remaining fields
  , _vertical   :: !HoledSeries
  , _horizontal :: !HoledSeries
  } deriving (Show, Eq, Generic, Ord)
makeLenses ''Field
instance NFData Field where rnf = genericRnf

-- | Get a new free field. All available stones are possible for the returned field.
newField :: Field
newField = Field (SS.fromList allStones) (HoledSeries mempty mempty) (HoledSeries mempty mempty)

-- | Get a list of all stones that could be laid on this field.
allPossibleStones :: Field -> SS.StoneSet
allPossibleStones = view possible

-- | Check whether a field is blocked. Being blocked means that the field has no possible stones.
blocked :: Field -> Bool
blocked f = SS.null (f^.possible)

-- | Check whether one could lay a stone on this field. This means that there is at least one possible stone.
free :: Field -> Bool
free = not . blocked

-- | Check whether a given position is inside the bounds of the board.
-- This function assumes that the board has the dimensions 16x16 and
-- the X- and Y-Axis start at both at 0.
inBoard :: Position -> Bool
inBoard (Position x y) | x >= 0 && x < 16 && y >= 0 && y < 16 = True
inBoard _ = False

-- | Get the holed series for the given axis.
axisHoledSeries :: Axis -> Lens' Field HoledSeries
axisHoledSeries XAxis = horizontal
axisHoledSeries YAxis = vertical
{-# INLINE axisHoledSeries #-}

-- | Get the stone series in the given direction of a field.
directionSeries :: Direction -> Lens' Field Series
directionSeries (Direction a x) = axisHoledSeries a.(if x then seriesAfter else seriesBefore)
{-# INLINE directionSeries #-}

-- | Get the length of the stone series in the given direction.
getSeriesLength :: Field -> Direction -> Int8
getSeriesLength f d = fromIntegral $ f^.directionSeries d.seriesStones.to SS.size
{-# INLINE getSeriesLength #-}

-- | Get the length of the holed series along the given axis.
-- The hole does not count.
getHoledSeriesLength :: Field -> Axis -> Int8
getHoledSeriesLength field axis = getSeriesLength field (Direction axis True) + getSeriesLength field (Direction axis False)
{-# INLINE getHoledSeriesLength #-}

-- | A board maps positions to fields.
newtype Board = Board (M.IntMap Field) deriving (Show, Eq, NFData, Ord)
-- We use the 'Enum' instance of 'Position' to convert the 'Position' into an 'Int'.
-- This allows us to use 'M.IntMap', which is much faster than an ordinary Map.

-- | Get a printable representation of the board.
displayBoard :: Board -> String
displayBoard (Board m) = unlines [ [maybe ' ' showField $ M.lookup (fromEnum $ Position x y) m | x <- [0..15]] | y <- [0..15] ]
  where showField f | blocked f = '#'
                    | otherwise = '+'

-- | Get a new empty board.
--
-- On an empty board, all fields will be free for all stones, but as soon as a stone
-- is laid onto the board, all the other free positions will be removed.
newBoard :: Board
newBoard = Board M.empty

-- | Create a new board where only a single position is free for all stones.
singleFieldBoard :: Position -> Board
singleFieldBoard = Board . flip M.singleton newField . fromEnum

-- | Check whether the board is empty.
isEmpty :: Board -> Bool
isEmpty (Board m) = M.null m

-- | Get the field at the given position of the board.
lookupPosition :: Position -> Board -> Maybe Field
lookupPosition p (Board b) | M.null b = Just newField
                           | otherwise = M.lookup (fromEnum p) b

-- | Delete the field at the given position of the board.
--
-- Note: Use with care. This function may corrupt the board.
deletePosition :: Position -> Board -> Board
deletePosition p (Board b) = Board $ M.delete (fromEnum p) b

-- | Get a list of all positions and fields in the board, on which stones could be laid.
allFreeFields :: Board -> [(Position, Field)]
allFreeFields (Board b)
  | M.null b = [(Position x y, newField) | x <- [0..15], y <- [0..15]]
  | otherwise = map (over _1 toEnum) $ filter (free . snd) $ M.toList b

-- | Updates the field in the given direction.
--
-- When inserting a stone, we need to update all the fields
-- at the end of the series into which we inserted the stone.
-- For example, we need to update the fields @A@ and @B@ and create the fields @D@ and @E@
-- when we insert a stone at position @C@ on the following board:
--
--       B
--       X
--    AXXCE
--       D
-- (X represents a stone)
--
-- We need to update them, because they all store the neighbour series
-- and field C is in a neighbour series of each of them.
--
-- This function will update the fields in one direction.
-- So @updateDirection f p s U@ will update the field B
-- and @updateDirection f p s D@ will create the field D
-- (f is the field C, p is the position of the field C, s is the stone to be inserted at that position).
--
-- Note: This function assumes that the stone actually fits at the specified place.
updateDirection :: Field -> Position -> Stone -> Direction -> Board -> Board
updateDirection f p s d = update series f p d
  where series :: Series
        series = fillSeriesHole s $ f^.axisHoledSeries (directionAxis d)

-- | The same as @updateDirection@ but it updates both fields along the axis.
--
-- This allows to share some work, so you should use this function if you plan to update in both directions
-- to get optimal performance.
updateAxis :: Field -> Position -> Stone -> Axis -> Board -> Board
updateAxis f p s a = update series f p (Direction a True) . update series f p (Direction a False)
  where series :: Series
        series = fillSeriesHole s $ f^.axisHoledSeries a

-- This is a helper function used by updateDirection and updateAxis.
update :: Series     -- ^ The series along the axis of the given direction with the stone already inserted.
       -> Field      -- ^ The field on which to place the new stone.
       -> Position   -- ^ Position of the field.
       -> Direction  -- ^ The direction in which we update.
       -> Board      -- ^ The board to update.
       -> Board      -- ^ The updated board.
update series f p d (Board b)
  -- Check that we don't create a new free field outside of the board's dimensions
  | inBoard p' = Board $ M.alter (Just . updatePossible . fromMaybe newField) (fromEnum p') b
  | otherwise  = Board b
 where
  -- The position of the field to update.
  p' :: Position
  p' = nextPosition d (getSeriesLength f d + 1) p

  updatePossible :: Field -> Field
  updatePossible fOld = f' & possible %~ go typeNew typeOrtho where
    -- Set the series in the direction from which we came to the joined series.
    f' = fOld & directionSeries (oppositeDirection d) .~ series

    -- The holed series before inserting the new stone.
    hsOld = fOld^.axisHoledSeries (directionAxis d)

    -- The holed series after inserting the new stone.
    hs = f'^.axisHoledSeries (directionAxis d)

    -- Calculate the type of a holed series.
    holedSeriesType h = h^.seriesBefore.seriesType <> h^.seriesAfter.seriesType

    typeOld = holedSeriesType hsOld
    typeNew = typeOld <> series^.seriesType
    typeOrtho = holedSeriesType $ fOld^.axisHoledSeries (orthogonalAxis $ directionAxis d)

    -- Check if we can join the "before" and "after" series after updating the field. To see why this
    -- is necessary, imagine the following board:
    --
    --   0  1  2  3 ...
    -- 0 Cm    Cb
    -- .
    -- .
    -- .
    --
    -- If we now insert the stone "Cm" on field (3|0) and update field (1|0), that
    -- field is now blocked because the new stone "Cm" collides with the already laid
    -- stone "Cm" on field (0|0). @canJoin@ will be false if this happens.
    -- This can also happen if any of the other "new stones" which are now in the "after series"
    -- by filling the hole at (3|0) collide with any stone of the before series (in this case, only Cm)
    canJoin = SS.null (SS.intersection (series^.seriesStones) (f'^.directionSeries d.seriesStones))

    -- Go calculates the possible stones of the field after updating it. We try to avoid
    -- the general case, because calculating the matching stones is expensive.

    -- Cases where there are no possible stones because the resulting series would be invalid:
    go Invalid        _               _          = mempty
    go (SameColor c)  (SameColor c')  _ | c /= c' = mempty
    go (SameSymbol s) (SameSymbol s') _ | s /= s' = mempty

    -- We need to check this now, because the next cases might generate invalid possible stones
    -- otherwise.
    go _              _               _ | not canJoin = mempty

    -- Cases where there is only a single or no possible stone after updating the series:
    go (SameColor c)  (SameSymbol s)  x          = singleStone (Stone s c) x
    go (SameSymbol s) (SameColor c)   x          = singleStone (Stone s c) x

    -- If the type of the series didn't change and we can join the before and after series (which we already checked if we get here),
    -- then it is enough to subtract the "new stones" in the series from the current possible stones.
    go _              _               x | typeOld == typeNew = x `SS.difference` (series^.seriesStones)

    -- General case
    go _              _               x = SS.intersection (matchingHoleStones hs) x

    -- Only a single stone is possible, because series of different types are intersecting. In this case, we still need to check
    -- that none of the series already contains that stone. We do that by checking that it's in the old possible
    -- stones and not in the stones that were added by the new joined series. If that's not the case, there is no possible stone.
    singleStone s ps
      | not (SS.member s $ series^.seriesStones) && SS.member s ps = SS.singleton s
      | otherwise = mempty


-- | Insert a stone at the given positon into the board.
--
-- Warning: Be careful that you insert the stone on a free field. Otherwise, the board
-- might get corrupted.
insertStone :: Position -> Stone -> Board -> Board
insertStone p s b = updateAxis f p s YAxis . updateAxis f p s XAxis $ deletePosition p b
  where f = fromMaybe newField $ lookupPosition p b

-- | Make the changes of a move visible in the board.
--
-- An exchange move will not change the board at all, while a lay move will put the laid stones
-- onto the board.
applyMove :: Move -> Board -> Board
applyMove (ExchangeMove _) b = b
applyMove (LayMove lays)   b = foldl' (\b' (st, p) -> insertStone p st b') b lays
