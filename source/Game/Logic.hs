{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
module Game.Logic
  ( Score(..), negateScore, Points
  , PartialMove(..), pointsForMoveLength, layStone, finalizeMove
  , buildMoves
  , allPossibleLayMoves
  , allPossibleExchangeMoves
  , allPossibleMoves
  ) where

import Control.DeepSeq.Generics
import Control.Lens
import Control.Monad
import Data.Function
import Data.Int
import Data.List (groupBy, sortBy)
import Data.Maybe
import Data.Monoid
import Data.Ord
import GHC.Generics

import Game.Board
import Game.Player
import Game.Position
import Game.Stone

import qualified Game.StoneSet as SS

data Score = Lost | Score Points | Won
           | None deriving (Show, Read, Eq, Generic)

instance NFData Score where rnf = genericRnf

instance Ord Score where
  compare Lost      Lost      = EQ
  compare Won       Won       = EQ
  compare None      None      = EQ
  compare Lost      _         = LT
  compare _         Lost      = GT
  compare Won       _         = GT
  compare _         Won       = LT
  compare (Score x) None      = compare x 0
  compare None      (Score x) = compare 0 x
  compare (Score x) (Score y) = compare x y

instance Monoid Score where
  mempty = None
  mappend None x = x
  mappend x None = x
  mappend Lost _ = Lost
  mappend _ Lost = Lost
  mappend Won _ = Won
  mappend _ Won = Won
  mappend (Score a) (Score b) = Score $ a + b

instance Bounded Score where
  minBound = Lost
  maxBound = Won

negateScore :: Score -> Score
negateScore None = None
negateScore Lost = Won
negateScore Won  = Lost
negateScore (Score x) = Score (-x)

data PartialMove = PartialMove {-# UNPACK #-} !Int8 [(Stone, Position)]
instance Monoid PartialMove where
  mempty = PartialMove 0 []
  mappend (PartialMove a b) (PartialMove a' b') = PartialMove (a + a') $ b' ++ b
    -- The last (++) is flipped to avoid quadric append cost on left associated use

pointsForMoveLength :: Int8 -> Int8
-- A stone must always be laid next to another stone, so if we get the length 1, that
-- means that there is no series in the current direction. For example, consider the following
-- board situation:
--
--  XA
--   A
--   A
--
-- Let AAA be a valid series, and X be a free field. If we're currently searching for moves
-- along the Y-Axis, and only lay a stone on the field X, the PartialMove for that move will
-- have the length 1. This means that without the following special case, we would get 1 point + 2 points
-- for the orthogonal 2-stone series, so 3 points in total, which is incorrect (it should be 2 points only).
--
-- The case also simplifies the calculation of the points for series that are orthogonal to the current
-- laying direction. If there are no neighbour stones in the orthogonal direction, we shouldn't add any points
-- for an orthogonal series. But because the current stone is part of the length of the orthogonal series, we add
-- add `pointsForMoveLength 1`, which would be `1` without the following case, and that is of course incorrect.
pointsForMoveLength 1 = 0
pointsForMoveLength 6 = 12
pointsForMoveLength x = x

layStone :: Stone -> Position -> Axis -> Field -> PartialMove
layStone stone position axis field = PartialMove pts [(stone, position)] where
  pts :: Int8
  pts = pointsForMoveLength $ getHoledSeriesLength field (orthogonalAxis axis) + 1

-- | Finalize a move, calculating the final score.
finalizeMove :: PartialMove -> Int8 -> (Points, Move)
finalizeMove (PartialMove p lays) l = (fromIntegral $ pointsForMoveLength l + p, LayMove lays)

data BuildState = BuildState
  { partialMove :: {-# UNPACK #-} !PartialMove
  , field :: {-# UNPACK #-} !Field
  , position :: {-# UNPACK #-} !Position
  , board :: Board
  , stoneBuffer :: {-# UNPACK #-} !([Stone], StoneCollection)
  , hand :: {-# UNPACK #-} !SS.StoneSet
  }

addStone :: Axis -> Stone -> BuildState -> BuildState
addStone axis stone (BuildState{..}) = BuildState partialMove' field position board' stoneBuffer' hand' where
  hand' :: SS.StoneSet
  hand' = SS.delete stone hand

  partialMove' :: PartialMove
  partialMove' = partialMove <> layStone stone position axis field

  stoneBuffer' :: ([Stone], StoneCollection)
  stoneBuffer' = takeStone stone stoneBuffer

  board' :: Board
  board' = insertStone position stone board

buildPossibleStones :: BuildState -> [Stone]
buildPossibleStones (BuildState{..}) = SS.toList $ SS.intersection hand $ allPossibleStones field

getCurrentMove :: Axis -> BuildState -> (Points, Move, Board, ([Stone], StoneCollection))
getCurrentMove axis (BuildState{..}) = (ps, move, board, stoneBuffer) where
  (ps, move) = finalizeMove partialMove $ 1 + getHoledSeriesLength field axis

buildMoveFocus :: Int8 -> Axis -> Direction -> BuildState -> Maybe BuildState
buildMoveFocus bound axis direction bs = do
  guard $ position' ^. coordinate axis > bound
  field' <- lookupPosition position' $ board bs
  return $ bs { field = field', position = position' }
 where
  position' = nextPosition direction (getSeriesLength (field bs) direction + 1) (position bs)

-- | Get a set of all moves together with the points for this move and the new board after performing the move,
--   which can be build from the given stones and start at the given position and are laid in the given direction.
buildMoves :: Int8 -> Axis -> Field -> Position -> Board -> ([Stone], StoneCollection) -> SS.StoneSet -> [(Points, Move, Board, ([Stone], StoneCollection))]
buildMoves bound axis f p b bu s
  | axis == XAxis = build False $ BuildState mempty f p b bu s
  | otherwise    = buildIgnore $ BuildState mempty f p b bu s
 where
  buildIgnore :: BuildState -> [(Points, Move, Board, ([Stone], StoneCollection))]
  buildIgnore bs = concatMap chooseStone $ buildPossibleStones bs where
    chooseStone :: Stone -> [(Points, Move, Board, ([Stone], StoneCollection))]
    chooseStone stone = go bs' (Direction axis True) ++ go bs' (Direction axis False) where
      bs' :: BuildState
      bs' = addStone axis stone bs

  go :: BuildState -> Direction -> [(Points, Move, Board, ([Stone], StoneCollection))]
  go bs direction = build (isForwards direction) <=< maybeToList $ buildMoveFocus bound axis direction bs

  build :: Bool -> BuildState -> [(Points, Move, Board, ([Stone], StoneCollection))]
  build stop bs = concatMap chooseStone $ buildPossibleStones bs where
    chooseStone :: Stone -> [(Points, Move, Board, ([Stone], StoneCollection))]
    chooseStone stone = getCurrentMove axis bs' : go bs' (Direction axis True) ++ (guard (not stop) >> go bs' (Direction axis False)) where
      bs' :: BuildState
      bs' = addStone axis stone bs

allPossibleLayMoves :: Board -> [Stone] -> StoneCollection -> [(Points, Move, Board, ([Stone], StoneCollection))]
allPossibleLayMoves board ns stc = moves
  where createMoves _ _ []          = []
        createMoves a b ((p,f):fps) = buildMoves b a f p board (ns, stc) (SS.fromList $ stoneList stc) ++ createMoves a (p^.coordinate a) fps

        moves = concatMap (createMoves XAxis $ -1) horizontalGroups ++ concatMap (createMoves YAxis $ -1) verticalGroups
        -- these groups rely on the facts that the allFreeFields function returns a list
        -- which is sorted into rows and that the sortBy function is stable.
        horizontalGroups = groupBy ((==) `on` (view yPosition . fst)) freeFields
        verticalGroups   = groupBy ((==) `on` (view xPosition . fst)) $ sortBy (comparing (view xPosition . fst)) freeFields
        freeFields = allFreeFields board

allPossibleExchangeMoves :: [Stone] -> StoneCollection -> [(Move, ([Stone], StoneCollection))]
allPossibleExchangeMoves ns stc = go (stoneList stc) (ns, stc) & mapped._1 %~ ExchangeMove where
  go []  _    = []
  go (x:xs) stonebuffer = ([x], takeStone x stonebuffer) : withIt ++ withoutIt where
    withIt = go xs (takeStone x stonebuffer) & mapped._1 %~ (x:)
    withoutIt = go xs stonebuffer

allPossibleMoves :: Board -> [Stone] -> StoneCollection -> [(Points, Move, Board, ([Stone], StoneCollection))]
allPossibleMoves b ns c = allPossibleLayMoves b ns c ++ map (\(m,c') -> (2, m, b, c')) (allPossibleExchangeMoves ns c)
