{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RankNTypes #-}
module Game.Position
  ( Position(..), xPosition, yPosition, nextPosition, coordinate
  , Axis(..), axisDirections, orthogonalAxis
  , Direction(..), allDirections, directionAxis, isForwards, directionDelta, oppositeDirection, orthogonalDirections, dUp, dDown, dLeft, dRight
  ) where

import Control.DeepSeq.Generics
import Control.Lens
import Data.Bits
import Data.Int
import Data.Monoid
import GHC.Generics
import Language.Haskell.TH.Lift

import OrphanInstances()

-- | A position in the 2D game grid.
data Position = Position
  { _xPosition :: {-# UNPACK #-} !Int8
  , _yPosition :: {-# UNPACK #-} !Int8
  } deriving (Eq, Read, Generic)
makeLenses ''Position
deriveLift ''Position
instance Show Position where
  showsPrec d (Position x y) = showParen (d > 10) $ showString "Position " . showsPrec 11 x . showString " " . showsPrec 11 y

instance NFData Position where rnf = genericRnf

instance Ord Position where
  compare (Position x1 y1) (Position x2 y2) = compare y1 y2 <> compare x1 x2

instance Enum Position where
  fromEnum (Position x y) = fromIntegral y `shiftL` 8 .|. fromIntegral x
  toEnum z = Position (fromIntegral x) (fromIntegral y)
    where y = 0xFF .&. z `shiftR` 8
          x = 0xFF .&. z

-- | An axis in our 2D grid can either be the X- or the Y-axis.
data Axis = XAxis | YAxis deriving (Show, Generic, Eq)
deriveLift ''Axis
instance NFData Axis where rnf = genericRnf

-- | Get a lens to the coordinate of the position on the specified axis.
coordinate :: Axis -> Lens' Position Int8
coordinate XAxis = xPosition
coordinate YAxis = yPosition
{-# INLINE coordinate #-}

-- | Get the orthogonal axis to the given axis.
orthogonalAxis :: Axis -> Axis
orthogonalAxis XAxis = YAxis
orthogonalAxis YAxis = XAxis

-- | A direction along either the X- or Y-axis.
data Direction = Direction Axis Bool deriving (Show, Generic)
  -- A direction stores the axis and whether to move forward or backward along that axis.
deriveLift ''Direction
instance NFData Direction where rnf = genericRnf

-- | Get the directions that point along the given axis.
axisDirections :: Axis -> [Direction]
axisDirections a = [ Direction a b | b <- [False, True] ]

-- | The axis along which movement is performed by the given direction.
directionAxis :: Direction -> Axis
directionAxis (Direction a _) = a

-- | A boolean indicating whether we are moving forwards or backwards on the axis.
isForwards :: Direction -> Bool
isForwards (Direction _ b) = b

-- | Get the increase in the coordinate for the given axis each time one step is made in the given direction.
directionDelta :: Direction -> Int
directionDelta (Direction _ True)  = 1
directionDelta (Direction _ False) = -1

-- | A list of all available directions.
allDirections :: [Direction]
allDirections = [dUp, dDown, dRight, dLeft]

dUp :: Direction
dUp = Direction YAxis True

dDown :: Direction
dDown = Direction YAxis False

dRight :: Direction
dRight = Direction XAxis True

dLeft :: Direction
dLeft = Direction XAxis False

oppositeDirection :: Direction -> Direction
oppositeDirection (Direction a f) = Direction a $ not f

orthogonalDirections :: Direction -> [Direction]
orthogonalDirections = axisDirections . orthogonalAxis . directionAxis

-- | Move a position @n@ steps in the given direction.
nextPosition :: Direction -> Int8 -> Position -> Position
nextPosition (Direction a f) = (if f then (+~) else (-~)) $ case a of
  XAxis -> xPosition
  YAxis -> yPosition
{-# INLINE nextPosition #-}
