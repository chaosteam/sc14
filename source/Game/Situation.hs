{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
module Game.Situation
  ( GameSituation(..), board, gameState, possibleNextSituations
  ) where

import Control.DeepSeq.Generics
import Control.Lens
import Data.Monoid
import GHC.Generics (Generic)

import qualified Data.Foldable as F

import Game.Board
import Game.Game
import Game.Logic
import Game.Player
import Game.Position
import Game.Stone

data GameSituation = GameSituation
                   { _board     :: Board
                   , _gameState :: GameState
                   } deriving Generic
makeLenses ''GameSituation
instance NFData GameSituation where rnf = genericRnf

newSituation :: Points -> Board -> [Stone] -> StoneCollection -> GameSituation -> GameSituation
newSituation p b next' collection' = set board b . over gameState update where
  update :: GameState -> GameState
  update gamestate = gamestate
                   & turn +~ 1
                   & nextPlayerData.points +~ p
                   & nextPlayerData.stones .~ collection'
                   & nextStones .~ next'
                   & nextColor %~ oppositePlayerColor

nextSituationsLay :: GameSituation -> [(Move, GameSituation)]
nextSituationsLay situation = [(m, newSituation p b ns' stc' situation) | (p, m, b, (ns', stc')) <- list] where
  list :: [(Points, Move, Board, ([Stone], StoneCollection))]
  list = allPossibleLayMoves (situation^.board) (situation^.gameState.nextStones) (situation^.gameState.nextPlayerData.stones)

nextSituationsExchange :: GameSituation -> [(Move, GameSituation)]
nextSituationsExchange situation = map (_2 %~ mkSituation) $ allPossibleExchangeMoves (situation^.gameState.nextStones) (situation^.gameState.nextPlayerData.stones) where
  mkSituation :: ([Stone], StoneCollection) -> GameSituation
  mkSituation (ns, stc) = newSituation 2 (situation^.board) ns stc situation

allPossibleNextSituations :: GameSituation -> [(Move, GameSituation)]
allPossibleNextSituations = nextSituationsLay <> nextSituationsExchange

startPossibleNextSituations :: GameSituation -> [(Move, GameSituation)]
startPossibleNextSituations = layMoves <> nextSituationsExchange where
  layMoves :: GameSituation -> [(Move, GameSituation)]
  layMoves = F.foldMap movesForPosition [Position 15 15, Position 8 15, Position 8 8]

  movesForPosition :: Position -> GameSituation -> [(Move, GameSituation)]
  movesForPosition pos = nextSituationsLay . set board (singleFieldBoard pos)

possibleNextSituations :: GameSituation -> [(Move, GameSituation)]
possibleNextSituations situation
  | isEmpty $ situation^.board = filterStart $ startPossibleNextSituations situation
  | otherwise = allPossibleNextSituations situation
 where
  filterStart :: [(Move, GameSituation)] -> [(Move, GameSituation)]
  filterStart = filter (isValidStartMove . fst)
