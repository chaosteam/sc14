{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}
module Client.Protocol where

import Control.Applicative
import Control.Error
import Control.Lens (over, _1, (%~), (&), mapped)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Loops
import Control.Monad.Trans.State
import Data.Conduit
import Data.Traversable (for)
import Data.Word
import Data.XML.Types
import Safe
import System.CPUTime
import Text.Printf

import qualified Data.Map as M
import qualified Data.Text as T

import Client.Error
import Client.LaxXML
import Game.Game
import Game.Player
import Game.Position
import Game.Replay
import Game.Stone

type Identifier = Word64
type Room = T.Text

--------------------------------------------------------------------------------
parseWelcome :: Parser' (Room, PlayerColor)
parseWelcome = do
  r <- tag "joined" $ textAttribute "roomId"
  c <- parent "room" ("roomId" =?= r) $
        tag "data" $ "class" =?= "welcome" *> attribute "color" readPlayerColorL
  return (r,c)

stoneAttributes :: AttributeParser Stone
stoneAttributes = Stone <$> attribute "shape" readStoneSymbol <*> attribute "color" readStoneColor

stoneAttributesWithId :: AttributeParser (Stone, Identifier)
stoneAttributesWithId = (,) <$> stoneAttributes <*> readAttribute "identifier"

parseStone :: Parser' Stone
parseStone = tag "stone" stoneAttributes

parseStoneWithId :: Parser' (Stone, Identifier)
parseStoneWithId = tag "stone" stoneAttributesWithId

parseField :: Monad m => (Position -> Parser o m a) -> Parser o m a
parseField = element "field" $ Position <$> readAttribute "posX" <*> readAttribute "posY"

parseMove :: Parser' Move
parseMove = element "move" (textAttribute "type") parse
  where parse :: T.Text -> Parser' Move
        parse "EXCHANGE" = ExchangeMove <$> untilEOF (tag "select" stoneAttributes)
        parse "LAY"      = LayMove      <$> untilEOF (parentName "stoneToField" $ (,) <$> parseStone <*> parseField return)
        parse t          = left $ AttributeParseFailed "type" t

parseNextStones :: Int -> Parser' [Stone]
parseNextStones n = parentName "nextStones" $ replicateM (min n 12) parseStone

parsePlayerStates :: PlayerColor -> AttributeParser a -> Parser' (PlayerData (PlayerState, a), M.Map Stone [Identifier])
parsePlayerStates sc extraAttributes = makePlayerData
  <$> element "red"  ((,) <$> readAttribute "points" <*> extraAttributes) parsePlayerState
  <*> element "blue" ((,) <$> readAttribute "points" <*> extraAttributes) parsePlayerState

  where parsePlayerState :: (Points, a) -> Parser' ((PlayerState, a), M.Map Stone [Identifier])
        parsePlayerState (p, extra) = do
          sts <- replicateM 6 parseStoneWithId
          let idMap = makeIdentifierMap sts
          return ((PlayerState p $ makeStoneCollection $ M.map length idMap, extra), idMap)

        makeIdentifierMap :: [(Stone, Identifier)] -> M.Map Stone [Identifier]
        makeIdentifierMap = M.fromListWith (++) . map ((:[]) <$>)

        makePlayerData :: (s, m) -> (s, m) -> (PlayerData s, m)
        makePlayerData (sr, mr) (sb, mb) = (PlayerData sr sb, if sc == PlayerRed then mr else mb)

parseInitialState :: Parser' (Room, GameConfiguration, GameState, M.Map Stone [Identifier])
parseInitialState = do
  (r,c) <- parseWelcome
  inRoom r $ parent "data" ("class" =?= "memento") $
    element "state" ("class" =?= "state" *> "turn" =?= "0" *> ((,) <$> readAttribute "stonesInBag" <*> attribute "current" readPlayerColorL)) $ \(n, first) -> do
      ns          <- parseNextStones n
      (ps, idMap) <- parsePlayerStates c $ return ()
      return
        ( r
        , GameConfiguration c first
        , GameState ns (fmap fst ps) first n 0
        , idMap
        )

parseGameResult :: Parser' GameResult
parseGameResult = tag "condition" (over reason readString <$> (GameResult <$> attribute "winner" readWinner <*> textAttribute "reason"))
  where readWinner "RED"  = Just $ Just PlayerRed
        readWinner "BLUE" = Just $ Just PlayerBlue
        readWinner "none" = Just Nothing
        readWinner _      = Nothing
        readString = read . T.unpack . T.cons '"' . flip T.snoc '"'

parseBoardStones :: Parser' [(Stone, Position)]
parseBoardStones = fmap catMaybes $ parentName "board" $ replicateM (16 * 16) $ parseField $ \pos -> fmap (,pos) <$> maybeEOF parseStone

inRoom :: Monad m => Room -> Parser o m a -> Parser o m a
inRoom r = parent "room" ("roomId" =?= r)

parseMemento :: PlayerColor -> GameConfiguration -> Parser o IO a -> Parser o IO (Either GameResult (Move, GameState, M.Map Stone [Identifier], a))
parseMemento cur gc parseBody =
  parent "data" ("class" =?= "memento") $
     untilJust $ element "state" ("class" =?= "state" *> attrs) $ \(n,t,cur') -> if cur /= cur'
       then do
         liftIO $ putStrLn "Checking for winner ..."
         result <- maybeEOF parseGameResult
         return $ Left <$> result
       else do
         start <- liftIO getCPUTime
         liftIO $ putStrLn "Parsing enemy move ..."

         ns          <- parseNextStones n
         (ps, idMap) <- parsePlayerStates (selfColor gc) $ return ()

         a <- parseBody

         liftIO $ putStrLn "Parsing last move ..."
         lm <- parseMove

         liftIO $ putStrLn "Checking for winner ..."
         result <- maybeEOF parseGameResult
         end <- liftIO getCPUTime

         void $ liftIO $ printf "Took %.3f seconds to parse enemy response.\n" $ fromInteger (end - start) / (10^(12 :: Int) :: Double)
         return $ Just $ maybe (Right (lm, GameState ns (fmap fst ps) cur n t, idMap, a)) Left result

   where attrs = (,,) <$> readAttribute "stonesInBag" <*> readAttribute "turn" <*> attribute "current" readPlayerColorL

parseEnemyAction :: Room -> GameConfiguration -> Parser o IO (Either GameResult (Move, GameState, M.Map Stone [Identifier]))
parseEnemyAction r gc = fmap (fmap to3tuple) $ inRoom r $ parseMemento (selfColor gc) gc (return ())
  where to3tuple (a,b,c,()) = (a,b,c)

parseEnemyActionBoard :: Room -> GameConfiguration -> Parser o IO (Either GameResult (Move, GameState, M.Map Stone [Identifier], [(Stone, Position)]))
parseEnemyActionBoard r gc = inRoom r $ parseMemento (selfColor gc) gc parseBoardStones

waitForMoveRequest :: Room -> Parser' ()
waitForMoveRequest r =
  parent "room" ("roomId" =?= r) $
    tag "data" $ "class" =?= "sc.framework.plugins.protocol.MoveRequest"

parseProtocol :: Monad m => Parser o m a -> Parser o m a
parseProtocol = (tagOpen "protocol" >>)

playerNameAttribute :: AttributeParser String
playerNameAttribute = T.unpack <$> textAttribute "displayName"

parseSituation :: Monad m => AttributeParser a -> Parser o m b -> Int -> Parser o m (PlayerData a, GameState, [(Stone, Position)], b)
parseSituation playerAttributes parseExtra t = element "state" (attributeValue "turn" (T.pack $ show t) *> attrs) $ \(nextStonesCount, current) -> do
  nextstones <- parseNextStones nextStonesCount
  (playerdata, _) <- parsePlayerStates current playerAttributes
  laidstones <- parseBoardStones
  extra <- parseExtra
  return (fmap snd playerdata, GameState nextstones (fmap fst playerdata) current nextStonesCount t, laidstones, extra)
  where attrs = (,) <$> readAttribute "stonesInBag" <*> attribute "current" readPlayerColorL

parseReplay :: Parser' Replay
parseReplay = parentName "object-stream" $ do
  (playernames, initgamestate, _, maybeResult) <- parseSituation playerNameAttribute (maybeEOF parseGameResult) 0
  (remainingSituations, result) <- maybe (go initgamestate 1) (return . ([],) . (initgamestate,)) maybeResult
  return $ Replay remainingSituations result playernames
  where go gamestate i = do
          (_, gamestate', laidstones, (m, maybeResult)) <- parseSituation (return ()) ((,) <$> parseMove <*> maybeEOF parseGameResult) i
          let situation = GameSnapshot m gamestate laidstones
          case maybeResult of
            Nothing     -> go gamestate' (i + 1) & mapped._1 %~ (situation :)
            Just result -> return ([situation], (gamestate', result))

--------------------------------------------------------------------------------
sendElement :: Monad m => Name -> [(Name, T.Text)] -> Conduit i m Event -> Conduit i m Event
sendElement name attributes body = do
  yield $ EventBeginElement name $ map (\(n,c) -> (n, [ContentText c])) attributes
  body
  yield $ EventEndElement name

sendTag :: Monad m => Name -> [(Name, T.Text)] -> Producer m Event
sendTag name attributes = sendElement name attributes $ return ()

sendStone :: Monad m => Name -> (Stone, Identifier) -> Producer m Event
sendStone n (Stone s c, i) = sendTag n
  [ ("color"      , showStoneColor c)
  , ("shape"      , showStoneSymbol s)
  , ("identifier" , T.pack $ show i)
  ]

sendField :: Monad m => Position -> Producer m Event
sendField (Position x y) = sendTag "field"
  [ ("posX", T.pack $ show x)
  , ("posY", T.pack $ show y)
  ]

sendMove :: Monad m => M.Map Stone [Identifier] -> Move -> Producer m Event
sendMove idMap (ExchangeMove xs) = sendElement "data" [("class", "exchangemove")] $ mapM_ (sendStone "select") unique
  where unique = flip evalState idMap $ for xs $ \st -> do
          sts <- get
          let (ids, sts') = M.insertLookupWithKey (\_ _ -> tailSafe) st undefined sts
          put sts'
          return (st, headNote ("While creating unique move: The player doesn't own some stone used in the move. Missing stone: " ++ show st) $ fromMaybe [] ids)
sendMove idMap (LayMove lays) = sendElement "data" [("class", "laymove")] $ mapM_ sendStoneToField unique
  where sendStoneToField (s,p) = sendElement "stoneToField" [] $ sendStone "stone" s >> sendField p
        unique = (\(s,p) -> ((s, headNote ("Missing sendMove stone: " ++ show s) $ fromMaybe [] $ M.lookup s idMap), p)) <$> lays

sendDefaultMove :: Monad m => M.Map Stone [Identifier] -> Producer m Event
sendDefaultMove idMap = sendElement "data" [("class", "exchangemove")] $ sendStone "select" (st,i)
  where (st, ids) = headNote "Empty stone id map" $ M.toList idMap
        i = headNote "Empty entry in stone id map" ids

toRoom :: Monad m => Room -> Conduit i m Event -> Conduit i m Event
toRoom r = sendElement "room" [("roomId", r)]

sendJoin :: Monad m => Maybe Room -> Producer m Event
sendJoin Nothing  = sendTag "join" [("gameType", "swc_2014_sixpack")]
sendJoin (Just c) = sendTag "joinPrepared" [("reservationCode", c)]

sendProtocolStart :: Monad m => Producer m Event
sendProtocolStart = yield $ EventBeginElement "protocol" []

--------------------------------------------------------------------------------
showStoneColor :: StoneColor -> T.Text
showStoneColor Blue = "BLUE"
showStoneColor Green = "GREEN"
showStoneColor Magenta = "MAGENTA"
showStoneColor Orange = "ORANGE"
showStoneColor Violet = "VIOLET"
showStoneColor Yellow = "YELLOW"

readStoneColor :: T.Text -> Maybe StoneColor
readStoneColor "BLUE" = Just Blue
readStoneColor "GREEN" = Just Green
readStoneColor "MAGENTA" = Just Magenta
readStoneColor "ORANGE" = Just Orange
readStoneColor "VIOLET" = Just Violet
readStoneColor "YELLOW" = Just Yellow
readStoneColor _ = Nothing

readStoneColorL :: T.Text -> Maybe StoneColor
readStoneColorL = readStoneColor . T.toUpper

showStoneSymbol :: StoneSymbol -> T.Text
showStoneSymbol Acorn = "ACORN"
showStoneSymbol Bell = "BELL"
showStoneSymbol Clubs = "CLUBS"
showStoneSymbol Diamond = "DIAMOND"
showStoneSymbol Heart = "HEART"
showStoneSymbol Spades = "SPADES"

readStoneSymbol :: T.Text -> Maybe StoneSymbol
readStoneSymbol "ACORN" = Just Acorn
readStoneSymbol "BELL" = Just Bell
readStoneSymbol "CLUBS" = Just Clubs
readStoneSymbol "DIAMOND" = Just Diamond
readStoneSymbol "HEART" = Just Heart
readStoneSymbol "SPADES" = Just Spades
readStoneSymbol _ = Nothing

showPlayerColor :: PlayerColor -> T.Text
showPlayerColor PlayerRed = "RED"
showPlayerColor PlayerBlue = "BLUE"

readPlayerColor :: T.Text -> Maybe PlayerColor
readPlayerColor "RED" = Just PlayerRed
readPlayerColor "BLUE" = Just PlayerBlue
readPlayerColor _ = Nothing

readPlayerColorL :: T.Text -> Maybe PlayerColor
readPlayerColorL = readPlayerColor . T.toUpper
