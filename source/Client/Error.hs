{-# LANGUAGE OverloadedStrings #-}
module Client.Error
  ( Error(..)
  , showError
  , exitOnError
  , errorT
  ) where

import Control.Applicative
import Control.Error
import Control.Monad.IO.Class
import Data.Monoid
import Data.XML.Types
import System.Exit

import qualified Data.Text as T

data Error = EOF
           | AttributeParseFailed Name T.Text
           | MissingAttribute Name
           | Strict String
  deriving Show

showError :: Error -> T.Text
showError EOF = "Unexpected EOF."
showError (AttributeParseFailed a v) = "Failed to parse value for attribute " <> nameLocalName a <> ": " <> v <> "."
showError (MissingAttribute n) = "Missing attribute " <> nameLocalName n <> "."
showError (Strict msg) = "Error: " <> T.pack msg <> " (Failing because of strict mode)"

-- | Run a EitherT that may fail with an error message. If an error happens, exit with return
-- code 1.
exitOnError :: (Applicative m, MonadIO m) => EitherT Error m a -> m a
exitOnError = eitherT handleError return where
  handleError e = liftIO $ do
    errLn $ T.unpack $ showError e
    exitFailure

errorT :: Monad m => (String -> m a) -> EitherT Error m a -> m a
errorT f = flip eitherT return $ f . T.unpack . showError

