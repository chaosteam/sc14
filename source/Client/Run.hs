{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
module Client.Run
  ( loadReplay
  , StrategyProcess()
  , createStrategy
  , handleForked
  , killStrategy
  , computeStrategy
  , stopStrategy
  , getStrategyResult
  , tellEnemyMove
  ) where

import Control.Applicative
import Control.Concurrent.Async
import Control.DeepSeq
import Control.Exception.Lifted
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Either
import Control.Monad.Trans.State
import Data.Conduit
import Data.Conduit.Binary (sourceHandle)
import Data.Conduit.Zlib
import Data.IORef
import Foreign.Ptr
import Foreign.Storable
import Numeric.Lens
import System.Environment
import System.Exit
import System.IO
import System.IO.Error
import System.Mem
import System.Process
import Text.XML.Stream.Parse (parseBytes, def)

import qualified Data.ByteString as BS

import Client.Error
import Client.Protocol
import Game.Board
import Game.Game
import Game.Position
import Game.Replay
import Game.Stone
import Strategy.List
import Strategy.Strategy
import System.IPC.Util

import qualified System.IPC.Manager as Manager
import qualified System.IPC.Semaphore as Semaphore
import qualified System.IPC.SharedMem as SharedMem

import OrphanInstances()
import StorableInstances()

-- | Checks if a file is compressed with gzip.
isGZiped :: Handle -> IO Bool
isGZiped h = do
  before <- hTell h
  hSeek h AbsoluteSeek 0
  magic <- BS.hGet h 2
  hSeek h AbsoluteSeek before
  return $ magic == BS.pack [0x1f, 0x8b] -- 0x8b1f is gzip's magic number

loadReplay :: FilePath -> EitherT Error IO Replay
loadReplay file = bracket (lift $ openBinaryFile file ReadMode) (lift . hClose) $ \h -> do
  gziped <- lift $ isGZiped h
  sourceHandle h
    $= (if gziped then ungzip else awaitForever yield)
    $= parseBytes def
    $$ eitherT (lift . left) (lift . right) parseReplay

--------------------------------------------------------------------------------
data Shared = Shared
  { currentBestMove  :: SharedMem.SharedMem MaybeMove
  , currentGameState :: SharedMem.SharedMem GameState
  , lastEnemyMove    :: SharedMem.SharedMem Move
  , mayRun           :: Semaphore.Semaphore
  }

data StrategyProcess = StrategyProcess
  { shared        :: Shared
  , commandHandle :: Handle
  , processHandle :: ProcessHandle
  }

sharedManager :: Manager.Manager' Shared
sharedManager = Shared
  <$> lmap currentBestMove (Manager.sharedMemory "currentBestMove")
  <*> lmap currentGameState (Manager.sharedMemory "currentGameState")
  <*> lmap lastEnemyMove (Manager.sharedMemory "lastEnemyMove")
  <*> lmap mayRun (Manager.semaphore "mayRun" 0)

createStrategyProcess :: GameState -> IO StrategyProcess
createStrategyProcess gs = do
  (sha, names) <- Manager.create sharedManager
  poke (SharedMem.pointer $ currentGameState sha) gs
  exe <- getInstanceExecutable
  (Just stdinH, _, _, pH) <- createProcess (proc exe ("fork":names))
    { std_in = CreatePipe
    }
  return $ StrategyProcess sha stdinH pH

killStrategy :: StrategyProcess -> IO ()
killStrategy StrategyProcess{..} = do
  terminateProcess processHandle
  void $ waitForProcess processHandle
  Manager.close sharedManager shared
  Manager.unlink sharedManager shared

--------------------------------------------------------------------------------
data Command = Compute | Stop | HandleEnemy deriving Enum

enumChar :: Enum a => Iso' a Char
enumChar = from enum.adding (fromEnum 'a').enum

getCommand :: IO Command
getCommand = getChar <&> review enumChar

sendCommand :: Handle -> Command -> IO ()
sendCommand h c = hPutChar h (c^.enumChar) >> hFlush h

--------------------------------------------------------------------------------
-- | Main entry point for the forked strategy process.
strategyProcessMain :: [String] -> IO ()
strategyProcessMain names = do
  sha@Shared{..} <- Manager.open sharedManager names
  Manager.unlink sharedManager sha
  f  <- loadStrategy
  gc <- loadGameConfiguration
  gs <- peek $ SharedMem.pointer currentGameState
  b  <- loadBoard
  strategyReference <- newIORef $ f gc gs b
  handle (unless . isEOFError <*> throwIO) $ flip evalStateT Nothing $ forever $ do
    command <- liftIO getCommand
    stop
    case command of
      Stop    -> liftIO performGC
      Compute -> start mayRun $ evaluator (SharedMem.pointer currentBestMove) strategyReference
      HandleEnemy -> liftIO $ do
        move       <- peek (SharedMem.pointer lastEnemyMove)
        gamestate' <- peek (SharedMem.pointer currentGameState)
        modifyIORef strategyReference $ \strategy -> handleEnemy strategy gamestate' move
 where
  start :: Semaphore.Semaphore -> (Semaphore.Semaphore -> IO ()) -> StateT (Maybe (Async ())) IO ()
  start mayRun action = put . Just <=< liftIO $ do
    Semaphore.setToOne mayRun
    asyncWithUnmask $ \unmask -> unmask $ action mayRun

  stop :: StateT (Maybe (Async ())) IO ()
  stop = get >>= traverse (liftIO . cancel) >> put Nothing

  evaluator:: Ptr MaybeMove -> IORef Strategy -> Semaphore.Semaphore -> IO ()
  evaluator currentbestmove strategyReference sem = do
    strategy <- readIORef strategyReference
    forM_ (moves strategy) $ \(message, value, strategy') -> do
      void $ evaluate $ force value
      putStrLn $ "[STATISTIC] " ++ message
      Semaphore.withTry sem $ \continue -> do
        unless continue $ throwIO ThreadKilled
        mask_ $ poke currentbestmove (MaybeMove . Just $ value) >> writeIORef strategyReference strategy'

-- | Call this function in main to handle the switch between the strategy process runner
-- and the normal client.
handleForked :: IO ()
handleForked = do
  args <- getArgs
  case args of
    ("fork":args') -> strategyProcessMain args' >> exitSuccess
    _ -> return ()

loadStrategy :: IO StrategyCreator
loadStrategy = getLine >>= useStrategy

sendStrategy :: Handle -> String -> IO ()
sendStrategy = hPutStrLn

loadGameConfiguration :: IO GameConfiguration
loadGameConfiguration = getLine <&> \(a:b:_) -> GameConfiguration (enumChar # a) (enumChar # b)

sendGameConfiguration :: Handle -> GameConfiguration -> IO ()
sendGameConfiguration h (GameConfiguration a b) = hPutStrLn h [a^.enumChar,b^.enumChar]

loadStonePosition :: IO (Stone, Position)
loadStonePosition = getLine <&> \(s:x:y:_) -> (enumChar # s, Position (enumChar # x) (enumChar # y))

sendStonePosition :: Handle -> (Stone, Position) -> IO ()
sendStonePosition h (s, Position x y) = hPutStrLn h [s^.enumChar,x^.enumChar,y^.enumChar]

loadBoard :: IO Board
loadBoard = fmap toBoard $ readLn >>= \l -> forM [(1::Int)..l] $ const loadStonePosition where
  toBoard :: [(Stone, Position)] -> Board
  toBoard = flip applyMove newBoard . LayMove

sendBoard :: Handle -> [(Stone, Position)] -> IO ()
sendBoard h bs = hPrint h (length bs) >> mapM_ (sendStonePosition h) bs

createStrategy :: String -> GameConfiguration -> GameState -> [(Stone, Position)] -> IO StrategyProcess
createStrategy strategyName gc gs b = do
  strat <- createStrategyProcess gs
  sendStrategy (commandHandle strat) strategyName
  sendGameConfiguration (commandHandle strat) gc
  sendBoard (commandHandle strat) b
  hFlush $ commandHandle strat
  return strat

stopStrategy :: StrategyProcess -> IO ()
stopStrategy StrategyProcess{commandHandle} = sendCommand commandHandle Stop

computeStrategy :: StrategyProcess -> IO ()
computeStrategy StrategyProcess{commandHandle} = sendCommand commandHandle Compute

getStrategyResult :: StrategyProcess -> IO (Maybe Move)
getStrategyResult StrategyProcess { shared = Shared{currentBestMove, mayRun} } = do
  Semaphore.wait mayRun >> Semaphore.lock mayRun
  MaybeMove m <- peek (SharedMem.pointer currentBestMove)
  poke (SharedMem.pointer currentBestMove) $ MaybeMove Nothing
  return m

tellEnemyMove :: StrategyProcess -> Move -> GameState -> IO ()
tellEnemyMove StrategyProcess{ shared = Shared{..}, commandHandle } move gamestate = do
  poke (SharedMem.pointer lastEnemyMove) move
  poke (SharedMem.pointer currentGameState) gamestate
  sendCommand commandHandle HandleEnemy
