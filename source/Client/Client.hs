{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedStrings #-}
module Client.Client
  ( client
  ) where

import Control.Applicative
import Control.Concurrent
import Control.Error hiding ((??))
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Resource
import Control.Monad.Trans.State
import Data.Conduit
import Data.Monoid
import Data.Void
import Data.XML.Types
import System.Time.Monotonic

import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.IO as T

import Client.Error
import Client.LaxXML
import Client.Protocol
import Client.Run
import Game.Game
import Game.Stone
import System.IPC.Util

blockWidth :: Int
blockWidth = 50

startBlock :: String -> IO ()
startBlock name = putStrLn $ '\n' : header
  where header = start ++ replicate (blockWidth - length start) '-'
        start = "-- " ++ name ++ " "

endBlock :: IO ()
endBlock = putStrLn $ replicate blockWidth '-'

doTurn :: Room -> M.Map Stone [Identifier] -> StrategyProcess -> Parser Event IO Bool
doTurn room idMap sp = do
  liftIO $ do
    startBlock "Do turn"
    computeStrategy sp
    putStrLn "Waiting for move request ..."

  waitForMoveRequest room
  clock <- liftIO newClock

  -- Don't print anything now until the sendMove / sendDefaultMove, to avoid any overhead.
  liftIO $ threadDelay $ 1800 * 1000
  liftIO $ putStrLn "Got move request"
  r <- liftIO $ getStrategyResult sp

  r' <- case r of
    -- The Nothing case should be really fast, because we know that we already
    -- used most of our time. (> 1800 ms probably already used)
    Nothing -> True <$ do
      lift (toRoom room $ sendDefaultMove idMap)
      liftIO (putStrLn "Warning: Sending default move, because no move was produced by the supplied strategy.")
    Just m -> False <$ lift (toRoom room $ sendMove idMap m)
  liftIO endBlock
  t <- liftIO $ clockGetTime clock

  liftIO $ do
    startBlock "Timing"
    putStrLn $ "Total time: " ++ show t
    endBlock

    putStrLn "Stopping strategy ..."
    stopStrategy sp

  return r'

client :: String -> Maybe T.Text -> Bool -> Parser Event (ResourceT IO) GameResult
client strategy res strict = do

  liftIO $ startBlock "Join"
  lift $ sendProtocolStart >> sendJoin res
  liftIO $ putStrLn "Sent join game message. Awaiting begin of server response ..."

  liftIO $ do
    changed <- setHighPriority
    when changed $ putStrLn "Adjusted process priority."

  parseProtocol $ do
    liftIO $ putStrLn "Got protocol start from server."
    (room, gameConfiguration, initGameState, initIdentifierMap) <- mapEitherT (transPipe lift) parseInitialState

    liftIO $ T.putStrLn $ "Assigned color " <> showPlayerColor (selfColor gameConfiguration)
    liftIO endBlock

    (keyStart, strategyProcess) <- lift $ allocate (createStrategy strategy gameConfiguration initGameState []) killStrategy

    -- If we have the first turn, do one move, so that the enemy always
    -- has the turn in the following code.
    reinit <- if selfColor gameConfiguration == startColor gameConfiguration
      then mapEitherT (transPipe lift) $ doTurn room initIdentifierMap strategyProcess
      else return False

    fmap (either id absurd) $ runEitherT $ flip evalStateT (strategyProcess, reinit, keyStart) $ forever $ do
      liftIO $ startBlock "Wait turn"
      (sp, r, key) <- get
      (sp', idMap, key') <- lift $ if r
        then do
          when strict $ lift $ left $ Strict "Strategy crashed"
          liftIO $ putStrLn "## ALERT ## RESETTING STRATEGY [This is usually a big disadvantage, because all cached values are lost]"
          liftIO $ release key
          (_, gs, idMap, b) <- mapEitherT (mapEitherT $ transPipe lift) $ parseMaybe $ parseEnemyActionBoard room gameConfiguration
          (key', sp') <- lift $ lift $ allocate (createStrategy strategy gameConfiguration gs b) killStrategy
          return (sp', idMap, key')
        else do
          (m, gs, idMap) <- mapEitherT (mapEitherT $ transPipe lift) $ parseMaybe $ parseEnemyAction room gameConfiguration
          liftIO $ tellEnemyMove sp m gs
          return (sp, idMap, key)
      liftIO endBlock
      r' <- lift $ lift $ mapEitherT (transPipe lift) $ doTurn room idMap sp'
      put (sp', r', key')

  where parseMaybe        = either (stop . left) right . fromMaybe (Left defaultGameResult) <=< lift . maybeEOF
        stop action       = liftIO endBlock >> liftIO (putStrLn "") >> action
        defaultGameResult = GameResult Nothing $ T.pack "The game ended, but the server did not send the game result."
