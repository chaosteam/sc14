{-# LANGUAGE ParallelListComp #-}
module Data.MemoStatic where

import Control.Monad
import Data.Traversable (traverse)
import Language.Haskell.TH

conName :: Con -> Maybe Name
conName (NormalC n []) = Just n
conName _ = Nothing

-- | @static ''EnumType [| f |]@ will generate a static lookup table for values returned by f where the indices are the constructors
-- of @EnumType@. For example, @$(static ''Bool [| f |])@ will generate code similar to:
--
-- @
--     let r1 = f False
--         r2 = f True
--     in \b -> case b of
--       False -> r1
--       True  -> r2
-- @
--
-- This will only call f False and f True once, and then memoize the result.
static :: Name -> ExpQ -> ExpQ
static n f = do
  i <- reify n
  cons <- case i of
    TyConI (DataD    _ _ [] c _) | Just c' <- traverse conName c    -> return c'
    TyConI (NewtypeD _ _ [] c _) | Just c' <- conName c             -> return [c']
    _ -> fail "Unsupported input data type. Only simple enumeration types are supported."
  vnames <- replicateM (length cons) $ newName "v"
  xname <- newName "x"
  f' <- f
  return $ LetE
    [ ValD (VarP vn) (NormalB $ AppE f' $ ConE cn) [] | cn <- cons | vn <- vnames ]
    $ LamE [VarP xname] $ CaseE (VarE xname) [ Match (ConP cn []) (NormalB $ VarE vn) [] | cn <- cons | vn <- vnames ]

staticLit :: (Integral a) => [a] -> ExpQ -> ExpQ
staticLit vs f = do
  let lits = map (IntegerL . toInteger) vs
  vnames <- replicateM (length lits) $ newName "v"
  xname <- newName "x"
  f' <- f
  return $ LetE
    [ ValD (VarP vn) (NormalB $ AppE f' $ LitE l) [] | l <- lits | vn <- vnames ]
    $ LamE [VarP xname] $ CaseE (VarE xname) [ Match (LitP l) (NormalB $ VarE vn) [] | l <- lits | vn <- vnames ]
