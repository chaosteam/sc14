module Strategy.Strategy
  ( Strategy(..)
  , nullStrategy
  , StrategyCreator
  ) where

import Game.Board
import Game.Game
import Game.Stone

data Strategy = Response
  { handleEnemy  :: GameState -> Move -> Strategy
  , moves        :: [(String, Move, Strategy)]
  }

nullStrategy :: Strategy
nullStrategy = Response (\_ _ -> nullStrategy) []

type StrategyCreator = GameConfiguration -> GameState -> Board -> Strategy
