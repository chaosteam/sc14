{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Strategy.AlphaBeta
  ( alphaBeta
  , principalVariation
  , showTree
  , GameTree(..)
  , SixpackTree
  , calculateScore
  , updateTree
  , extendSituation
  , strategy
  , Statistic()
  , showStatistic
  , StatisticLogger
  , runLogger
  , evaluateLogger
  , chooseMove
  ) where

import Control.Applicative
import Control.DeepSeq
import Control.DeepSeq.Generics
import Control.Lens hiding (Choice, (<|))
import Control.Monad.RWS.Strict hiding ((<>))
import Data.List.NonEmpty (NonEmpty(..), (<|), nonEmpty)
import Data.Maybe
import Data.Semigroup
import GHC.Generics

import qualified Data.Foldable as F
import qualified Data.IntMap as M
import qualified Data.List.NonEmpty as NE

import Game.Board
import Game.Game
import Game.Logic
import Game.Player
import Game.Situation
import Game.Stone
import Strategy.Strategy hiding (moves)

import OrphanInstances()

type Node   m l = (m, GameTree m l)
type Choice m l = NonEmpty (Node m l)

data GameTree m l = Fork Score (Choice m l)
                  | Leaf Score l
  deriving (Show, Read, Generic)
instance (NFData m, NFData l) => NFData (GameTree m l) where rnf = genericRnf

instance Functor (GameTree m) where
  fmap f (Leaf s x)  = Leaf s $ f x
  fmap f (Fork s cs) = Fork s $ cs & traverse._2.mapped %~ f

instance Applicative (GameTree m) where
  pure = return
  (<*>) = ap

instance Monad (GameTree m) where
  return = Leaf None
  Leaf _ x  >>= f = f x
  Fork _ cs >>= f = Fork None $ cs & traverse._2 %~ (>>= f)

showTree :: (Show m, Show l) => GameTree m l -> String
showTree = unlines . go
  where go (Leaf score leaf) = ["Leaf " ++ show score ++ " " ++ show leaf]
        go (Fork score cs)   = concat [ (show move ++ " " ++ show score) : map ("  " ++) (go subtree)
                                      | (move, subtree) <- NE.toList cs ]

principalVariation :: GameTree m l -> [m]
principalVariation (Leaf _ _ ) = []
principalVariation (Fork _ ((move, subtree) :| _)) = move : principalVariation subtree

treeScore :: Lens' (GameTree m l) Score
treeScore f (Leaf score x) = (Leaf ?? x) <$> f score
treeScore f (Fork score x) = (Fork ?? x) <$> f score
{-# INLINE treeScore #-}

scoreOf :: Node m l -> Score
scoreOf = view $ _2.treeScore

insert :: Node m l -> Choice m l -> Choice m l
insert node (node' :| nodes)
  | scoreOf node >= scoreOf node' = node  :| node' : nodes
  | otherwise                    = node' <| maybe (return node) (insert node) (nonEmpty nodes)

data Statistic = Statistic
  { cutoffs        :: M.IntMap Int
  , searchedLeaves :: Int
  , minimalDepth   :: Maybe (Min Int)
  , maximalDepth   :: Maybe (Max Int)
  , minimalScore   :: Maybe (Min Score)
  , maximalScore   :: Maybe (Max Score)
  }

instance Semigroup Statistic where
  (Statistic a b c d e f) <> (Statistic a' b' c' d' e' f') = Statistic (M.unionWith (+) a a') (b + b') (c <> c') (d <> d') (e <> e') (f <> f')

instance Monoid Statistic where
  mempty  = Statistic M.empty 0 Nothing Nothing Nothing Nothing
  mappend = (<>)

showStatistic :: Statistic -> String
showStatistic statistic = "Leaves searched: " ++ show (searchedLeaves statistic)
                     ++ "; Depth: " ++ show (fromMaybe 0 $ getMin <$> minimalDepth statistic)
                             ++ "/" ++ show (fromMaybe 0 $ getMax <$> maximalDepth statistic)
                     ++ "; Score: " ++ show (F.fold $ getMin <$> minimalScore statistic)
                             ++ "/" ++ show (F.fold $ getMax <$> maximalScore statistic)
                     ++ "; Cutoffs: " ++ unwords (map (\(d,c) -> show d ++ ":" ++ show c) $ M.toList $ cutoffs statistic)

type StatisticLogger = RWS Int Statistic ()

cutoffStatistic :: StatisticLogger ()
cutoffStatistic = do
  depth <- ask
  tell $ Statistic (M.singleton depth 1) 0 Nothing Nothing Nothing Nothing

leafStatistic :: Score -> StatisticLogger ()
leafStatistic score = do
  depth <- ask
  tell $ Statistic M.empty 1 (Just $ Min depth) (Just $ Max depth) (Just $ Min score) (Just $ Max score)

increaseDepth :: StatisticLogger a -> StatisticLogger a
increaseDepth = local (+1)

runLogger :: StatisticLogger a -> (a, Statistic)
runLogger m = evalRWS m 0 ()

evaluateLogger :: StatisticLogger a -> a
evaluateLogger = fst . runLogger

alphaBeta :: forall l m . (l -> Score) -> GameTree m l -> StatisticLogger (GameTree m l)
alphaBeta f = go False minBound maxBound

  where go :: Bool -> Score -> Score -> GameTree m l -> StatisticLogger (GameTree m l)
        go _       _     _    (Leaf _ x)       = let score = f x in Leaf score x <$ leafStatistic score
        go isEnemy alpha beta (Fork _ choices) = rebuildChoice isEnemy <$> sortNodes isEnemy alpha beta choices

        rebuildChoice :: Bool -> ([Node m l], Choice m l) -> GameTree m l
        rebuildChoice isEnemy (nodes, node :| rest) = Fork (adjust isEnemy $ scoreOf node) $ node :| rest ++ nodes

        sortNodes :: Bool -> Score -> Score -> Choice m l -> StatisticLogger ([Node m l], Choice m l)
        sortNodes !isEnemy !alpha !beta ((move, tree) :| nodes) = do
          tree' <- increaseDepth $ go (not isEnemy) (negateScore beta) (negateScore alpha) tree
          let (score', node) = (move, ) <$> (tree' & treeScore <%~ adjust isEnemy)
          if score' >= beta
            then (nodes, return node) <$ cutoffStatistic
            else case nonEmpty nodes of
              Nothing       -> return ([], return node)
              (Just nodes') -> do
                other <- sortNodes isEnemy (max score' alpha) beta nodes'
                return $ other & _2 %~ insert node

        adjust isEnemy = if isEnemy then negateScore else id

type SixpackTree = GameTree Move GameSituation

updateTree :: [Stone] -> SixpackTree -> SixpackTree
updateTree = updateTree' 0 0

updateTree' :: Int -> Int -> [Stone] -> SixpackTree -> SixpackTree
updateTree' used used' nextstones (Leaf s situation) = Leaf s $ situation & gameState %~ updateStones
  where updateStones gamestate = gamestate
                               & nextPlayerData.stones .~ stonecollection
                               & currentPlayerData.stones .~ stonecollection'
                               & nextStones .~ nextstones''
          where (nextstones' , stonecollection ) = refillStones used  nextstones  $ gamestate^.nextPlayerData.stones
                (nextstones'', stonecollection') = refillStones used' nextstones' $ gamestate^.currentPlayerData.stones
updateTree' used used' nextstones (Fork s cs) = Fork s $ NE.map update cs
  where update (move, tree) = (move, updateTree' used' (length $ usedStones move) (drop used nextstones) tree)

extendSituation :: GameSituation -> SixpackTree
extendSituation situation
    | extendable = rebuild (Leaf None) $ possibleNextSituations situation
    | otherwise  = Leaf None situation

  where rebuild _ []     = error "No possible next moves. This should never happen"
        rebuild f (x:xs) = Fork None $ over (mapped._2) f $ x :| xs

        extendable = isComplete (situation^.gameState.nextPlayerData.stones) && (situation^.gameState.turn < 40)

pruneExtend :: Int -> SixpackTree -> SixpackTree
pruneExtend _ (Leaf _ l)  = extendSituation l
pruneExtend d (Fork _ cs) = Fork None $ (if d >= 2 then NE.fromList . NE.take 30 else id) cs & traverse._2 %~ (>>= extendSituation)

calculateScore :: GameConfiguration -> GameSituation -> Score
calculateScore gc situation
    | lastTurn    = finalScore $ playerPoints compare
    | noMoreMoves = finalScore $ compare 0 $ lastPlayerAddition $ playerPoints (-)
    | otherwise   = Score $ playerPoints (-)

  where lastTurn    = situation^.gameState.turn == 40
        noMoreMoves = null $ allFreeFields $ situation^.board

        playerPoints f = (situation^.gameState.players.self gc.points)
                     `f` (situation^.gameState.players.enemy gc.points)

        finalScore GT = maxBound
        finalScore EQ = mempty
        finalScore LT = minBound

        lastPlayerAddition x | situation^.gameState.nextColor == startColor gc = x
                             | startColor gc == selfColor gc                   = x - 2
                             | otherwise                                      = x + 2

chooseMove :: Bool -> SixpackTree -> GameState -> Move -> Maybe SixpackTree
chooseMove True _ gs move = Just $ Leaf None $ GameSituation (applyMove move newBoard) gs
chooseMove _ (Leaf _ situation) gs move = Just $ Leaf None $ situation & board %~ applyMove move & gameState .~ gs
chooseMove _ (Fork _ choices)   _  move = lookup move $ NE.toList choices

enemyHandler :: Bool -> SixpackTree -> GameConfiguration -> GameState -> Move -> Strategy
enemyHandler start tree gc gs move = maybe nullStrategy (alphaBetaStrategy start' gc gs . updateTree (gs^.nextStones)) . chooseMove start tree gs $ move where
  start' = start && has _ExchangeMove move

takeTill :: (a -> Bool) -> [a] -> [a]
takeTill f = go where
  go [] = []
  go (x:xs)
    | f x = [x]
    | otherwise = x : go xs

alphaBetaStrategy :: Bool -> GameConfiguration -> GameState -> SixpackTree -> Strategy
alphaBetaStrategy !start gc gs tree = Response (enemyHandler start tree gc) moves where
  next :: Statistic -> SixpackTree -> (SixpackTree, Statistic)
  next statistic = runLogger . alphaBeta (calculateScore gc) . pruneExtend (maybe 0 getMin $ minimalDepth statistic)

  extractMove :: SixpackTree -> Statistic -> (String, Move, Strategy)
  extractMove tree'@(Leaf _ _) _ = ("default move", defaultmove, alphaBetaStrategy True gc gs tree')
    where defaultmove = defaultMove $ gs^.players.self gc.stones
  extractMove (Fork _ ((move, tree'') :| _)) statistic = (showStatistic statistic, move, alphaBetaStrategy (start && has _ExchangeMove move) gc gs tree'')

  trees :: [(SixpackTree, Statistic)]
  trees = takeTill ((== 4) . maybe 0 getMin . minimalDepth . snd) $ iterate (uncurry $ flip next) (tree, mempty)
  {-# INLINE trees #-} -- Inline this to avoid sharing

  moves :: [(String, Move, Strategy)]
  moves = map (uncurry extractMove) trees
  {-# INLINE moves #-}

strategy :: StrategyCreator
strategy gc gs b = alphaBetaStrategy True gc gs $ Leaf None $ GameSituation b gs
