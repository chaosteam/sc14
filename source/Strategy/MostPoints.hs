{-# LANGUAGE BangPatterns #-}
module Strategy.MostPoints
  ( strategy
  ) where

import Control.Lens

import Game.Board
import Game.Game
import Game.Logic
import Game.Player
import Game.Stone
import Strategy.Strategy

strategy :: StrategyCreator
strategy gc gs !b = Response enemyHandler $ ("Default exchange move", defaultMove $ gs^.players.self gc.stones, strategy gc gs b) : search 2 list
  where search _ [] = []
        search pts ((p,m,b',_):xs)
          | p > pts = ("Points: " ++ show pts, m, strategy gc gs b') : search p xs
          | otherwise = search pts xs

        list = allPossibleLayMoves b (gs^.nextStones) $ gs^.players.self gc.stones
        enemyHandler gs' m = strategy gc gs' $ applyMove m b
