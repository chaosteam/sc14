module Strategy.List where

import Control.Lens
import Control.Monad
import Data.Functor
import System.Exit

import Game.Game
import Game.Player
import Game.Stone
import Strategy.Strategy

import qualified Strategy.AlphaBeta as AlphaBeta
import qualified Strategy.MostPoints as MostPoints

strategies :: [(String, StrategyCreator)]
strategies =
  [ ("alpha-beta", AlphaBeta.strategy)
  , ("most-points", MostPoints.strategy)
  , ("null", \_ _ _ -> nullStrategy)
  , ("repeat", \gc x _ -> let s gs = Response (\gs' _ -> s gs') $ repeat ("move", defaultMove $ gs^.players.self gc.stones, s gs) in s x)
  ]

defaultStrategyName :: IO String
defaultStrategyName = case strategies of
  ((n,_):_) -> n <$ putStrLn ("Using default strategy `" ++ n ++ "'")
  _         -> putStrLn "Error: No strategies available!" >> exitFailure

resolveStrategyName :: Maybe String -> IO String
resolveStrategyName Nothing = defaultStrategyName
resolveStrategyName (Just name) = name <$ do
  unless (elem name . map fst $ strategies) $ invalidStrategyError name
  putStrLn $ "Using strategy `" ++ name ++ "'."

invalidStrategyError :: String -> IO a
invalidStrategyError name = putStrLn ("Error: No strategy `" ++ name ++ "' exists.") >> exitFailure

useStrategy :: String -> IO StrategyCreator
useStrategy name = case lookup name strategies of
  Nothing -> invalidStrategyError name
  Just s' -> return s'
