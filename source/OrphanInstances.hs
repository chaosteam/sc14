{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module OrphanInstances where

import Control.DeepSeq
import Data.List.NonEmpty (NonEmpty(..))
import Language.Haskell.TH.Lift (deriveLift)

import Instances.TH.Lift()

deriveLift ''NonEmpty
instance NFData a => NFData (NonEmpty a) where
  rnf (a :| as) = rnf (a,as)
