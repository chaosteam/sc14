#!/usr/bin/env bash

git checkout deploy
git merge --no-ff master --no-edit
vagrant up
vagrant ssh -c '/bin/bash -l /vagrant/scripts/build-release.sh'
vagrant halt

dist/build/contest/contest upload dist-release/release.zip -n $(git rev-parse --short HEAD)

git push
git checkout master
