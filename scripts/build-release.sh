#!/usr/bin/env sh

cd /vagrant

if [ $# -gt "0" ]; then 
    name=$1 
    shift
else
    name=release
fi

cabal --sandbox-config-file=./release.cabal.sandbox.config sandbox init --sandbox=./.release-cabal-sandbox/
cabal --sandbox-config-file=./release.cabal.sandbox.config install --only-dependencies -j$ncpus --reorder-goals
cabal --sandbox-config-file=./release.cabal.sandbox.config --builddir=dist-release configure --ghc-options="-O2 -rtsopts" --ghc-option='-with-rtsopts=-A1M -H1000M' -f-contest
cabal --sandbox-config-file=./release.cabal.sandbox.config --builddir=dist-release build

mkdir -p dist-release/release
cd dist-release/release
cp ../build/client/client .

echo "Creating $name.zip"
zip $name client

mv -f $name.zip ../$name.zip
cd ..
rm -r release
