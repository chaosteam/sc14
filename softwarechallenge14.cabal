name:          softwarechallenge14
version:       0.1
cabal-version: >= 1.10
license-file:  LICENSE
author:        Benno Fünfstück, Felix Kunzmann
maintainer:    Benno Fünfstück <benno.fuenfstueck@gmail.com>
stability:     experimental
homepage:      http://github.com/bennofs/softwarechallenge14/
bug-reports:   http://github.com/bennofs/softwarechallenge14/issues
copyright:     Copyright (C) 2013-2014 Benno Fünfstück, Felix Kunzmann
synopsis:      softwarechallenge14
description:   softwarechallenge14
build-type:    Custom

extra-source-files:
  .ghci
  .gitignore
  .travis.yml
  .vim.custom
  README.md

source-repository head
  type: git

flag new-criterion
  default: False
  description:
    Use a newer criterion version.
    (This only works if you install a patched criterion/statistics version from git that works with binary-0.5.)

flag progression
  default: False
  manual: True
  description:
    Use the "progression" library for benchmarks (this allows for easy comparing of different versions).
    This required a patch to progression that is not yet on hackage.

flag contest
  default: True
  manual: True
  description: Build the contest tools

library
  hs-source-dirs: source
  default-language: Haskell2010
  ghc-options: -O2 -Wall -threaded -rtsopts
  if !os(windows)
    ld-options: -pthread
  exposed-modules:
    Client.Client
    Client.Error
    Client.LaxXML
    Client.Protocol
    Client.Run

    Game.Board
    Game.Game
    Game.Player
    Game.Position
    Game.Stone
    Game.Replay
    Game.Logic
    Game.Situation
    Game.StoneSet

    Strategy.Strategy
    Strategy.MostPoints
    Strategy.AlphaBeta
    Strategy.List

    StorableInstances
    OrphanInstances
    Data.MemoStatic
    System.IPC.Semaphore
    System.IPC.SharedMem
    System.IPC.Manager
    System.IPC.Util

  build-depends:
      base                      >= 4.4 && < 5
    , conduit                   == 1.1.*
    , conduit-extra             == 1.1.*
    , xml-conduit               == 1.2.*
    , xml-types                 == 0.3.*
    , transformers              == 0.3.*
    , lens                      == 4.2.*
    , either                    == 4.3.*
    , errors                    == 1.4.*
    , text                      >= 0.11 && < 1.2
    , mmorph                    == 1.0.*
    , containers                == 0.5.*
    , monad-loops               == 0.4.*
    , free                      == 4.9.*
    , safe                      == 0.3.*
    , semigroups                == 0.15.*
    , async                     == 2.0.*
    , deepseq                   == 1.3.*
    , deepseq-generics          == 0.1.*
    , time                      == 1.4.*
    , system-time-monotonic     == 0.2.*
    , void                      == 0.6.*
    , bytestring                == 0.10.*
    , resourcet                 == 1.1.*
    , lifted-base               == 0.2.*
    , th-lift                   == 0.6.*
    , th-lift-instances         == 0.1.*
    , mwc-random                == 0.13.*
    , exceptions                == 0.6.*
    , process                   >= 1.1 && < 1.3
    , template-haskell
    , mtl
  if os(windows)
    build-depends: Win32
    extra-libraries: "kernel32"
  else
    build-depends: unix

executable client
  hs-source-dirs: .
  main-is: Main.hs
  ghc-options: -O2 -threaded -rtsopts -with-rtsopts=-A1M
  if os(windows)
    extra-libraries: "kernel32"
  else
    ld-options: -pthread

  default-language: Haskell2010
  build-depends:
      base                      >= 4.4 && < 5
    , conduit                   == 1.1.*
    , xml-conduit               == 1.2.*
    , conduit-extra             == 1.1.*
    , network                   >= 2.3 && < 2.6
    , optparse-applicative      >= 0.8 && < 0.10
    , blaze-builder             == 0.3.*
    , transformers              == 0.3.*
    , either                    == 4.3.*
    , text                      >= 0.11 && < 1.2
    , bytestring                == 0.10.*
    , lens                      == 4.2.*
    , resourcet                 == 1.1.*
    , softwarechallenge14

test-suite hlint
  type: exitcode-stdio-1.0
  main-is: hlint.hs
  if !os(windows)
    ld-options: -pthread
  default-language: Haskell2010
  build-depends:
      base
    , hlint == 1.8.*
  ghc-options: -O2 -w -threaded -rtsopts -with-rtsopts=-N
  hs-source-dirs: tests

test-suite doctests
  type: exitcode-stdio-1.0
  main-is: doctests.hs
  if !os(windows)
    ld-options: -pthread
  default-language: Haskell2010
  build-depends:
      base
    , directory >= 1.0
    , doctest   >= 0.9.1
    , binary    == 0.5.*
    , filepath
  ghc-options: -O2 -Wall -threaded
  if impl(ghc<7.6.1)
    ghc-options: -Werror
  hs-source-dirs: tests

test-suite tests
  type: exitcode-stdio-1.0
  main-is: Main.hs
  if !os(windows)
    ld-options: -pthread
  other-modules:
    BoardTests
    AlphaBetaTests
    LogicTests
    ArbitraryInstances
    StorableTests
  default-language: Haskell2010
  build-depends:
      base              >= 4.4 && < 5
    , tasty             == 0.8.*
    , tasty-th          == 0.1.*
    , tasty-quickcheck  == 0.8.*
    , tasty-hunit       == 0.8.*
    , QuickCheck        == 2.7.*
    , HUnit             == 1.2.*
    , containers        == 0.5.*
    , either            == 4.3.*
    , transformers      == 0.3.*
    , lens              == 4.2.*
    , semigroups        == 0.15.*
    , quickcheck-property-monad == 0.2.*
    , softwarechallenge14
  hs-source-dirs: tests

benchmark benchmarks
  type: exitcode-stdio-1.0
  hs-source-dirs: benchmarks
  main-is: Main.hs
  default-language: Haskell2010
  ghc-options: -O2 -rtsopts "-with-rtsopts=-A1M -H990M"
  other-modules:
    BenchmarkData
    AlphaBetaBenchmarks
    BoardBenchmarks
    MostPointsBenchmarks
    Snapshots
    TH.Snapshots
  build-depends:
      base       >= 4.4 && < 5
    , containers == 0.5.*
    , lens       == 4.2.*
    , either     == 4.3.*
    , template-haskell
    , softwarechallenge14

  if flag(new-criterion)
    build-depends:
      criterion == 0.9.*
  else
    build-depends:
       criterion == 0.6.*
       -- Workaround for missing version constraint in criterion (we don't actually need statistics, hastache)
     , statistics == 0.10.3.*
     , hastache   == 0.5.*

  if flag(progression)
    build-depends:
      progression == 0.6.*
    cpp-options: -DUSE_PROGRESSION

benchmark cutoffs
  type: exitcode-stdio-1.0
  hs-source-dirs: benchmarks
  main-is: BenchmarkCutoffs.hs
  default-language: Haskell2010
  ghc-options: -O2 -rtsopts "-with-rtsopts=-A1M -H990M"
  other-modules:
    Snapshots
    TH.Snapshots
  build-depends:
      base       >= 4.4 && < 5
    , containers == 0.5.*
    , deepseq    == 1.3.*
    , lens       == 4.2.*
    , safe       == 0.3.*
    , template-haskell
    , softwarechallenge14

executable contest
  hs-source-dirs: contest
  main-is: Main.hs
  other-modules:
    API
    Browser
    Session
    Util
  build-depends:
      base                 >= 4.6 && < 5
    , lens                 == 4.2.*
    , wreq                 == 0.1.*
    , html-conduit         == 1.1.*
    , xml-lens             == 0.1.*
    , http-client          == 0.3.*
    , mtl                  == 2.1.*
    , bytestring           == 0.10.*
    , text                 >= 0.11 && < 1.2
    , optparse-applicative >= 0.8 && < 0.10
    , regex-applicative    == 0.3.*
    , filepath             == 1.3.*
    , profunctors          == 4.0.*
    , split                == 0.2.*
    , safe                 == 0.3.*
    , directory            == 1.2.*
    , old-locale           == 1.0.*
    , time                 == 1.4.*
    , async                == 2.0.*
  default-language: Haskell2010
  if !flag(contest)
    buildable: False
