{-# LANGUAGE TemplateHaskell #-}
module Snapshots (snapshots) where

import TH.Snapshots

import Game.Player
import Game.Replay

snapshots :: [(String, (PlayerColor, GameSnapshot))]
snapshots = $(fromReplay "data/1394320788_log_110160.xml.gz" $ zip (map return ['A'..'Z'])
  [ 2
  , 5
  , 10
  , 17
  , 19
  , 35
  , 39
  ])
