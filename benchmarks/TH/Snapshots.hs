{-# LANGUAGE RecordWildCards #-}
module TH.Snapshots where

import Control.Lens
import Language.Haskell.TH
import Language.Haskell.TH.Syntax

import Client.Error
import Client.Run
import Game.Game
import Game.Replay

fromReplay :: String -> [(String, Int)] -> ExpQ
fromReplay file turns = do
  addDependentFile file
  Replay{..} <- runIO $ errorT fail $ loadReplay file

  let f (name, t) = (name, (gameSnapshots ^?! ix 0.to gameState.nextColor, gameSnapshots ^?! ix t))
  lift $ map f turns
