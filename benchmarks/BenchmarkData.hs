module BenchmarkData where

import Control.Lens

import qualified Data.Map as M

import Game.Game
import Game.Player
import Game.Stone

startNextStones :: [Stone]
startNextStones = [ Stone Clubs Magenta
                  , Stone Heart Orange
                  , Stone Clubs Yellow
                  , Stone Diamond Green
                  , Stone Diamond Violet
                  , Stone Acorn Orange
                  , Stone Clubs Blue
                  , Stone Heart Blue
                  , Stone Clubs Yellow
                  , Stone Acorn Green
                  , Stone Diamond Green
                  , Stone Acorn Green
                  ]

redStones :: StoneCollection
redStones = makeStoneCollection $ M.fromList $ zip ?? repeat 1 $
  [ Stone Bell Violet
  , Stone Acorn Blue
  , Stone Clubs Blue
  , Stone Heart Green
  , Stone Spades Green
  , Stone Acorn Yellow
  ]

blueStones :: StoneCollection
blueStones = makeStoneCollection $ M.fromList $ zip ?? repeat 1 $
  [ Stone Bell Orange
  , Stone Clubs Magenta
  , Stone Bell Magenta
  , Stone Heart Yellow
  , Stone Clubs Orange
  , Stone Diamond Orange
  ]

startPlayerData :: PlayerData PlayerState
startPlayerData = PlayerData (PlayerState 0 redStones) (PlayerState 0 blueStones)

startGameState :: GameState
startGameState = GameState startNextStones startPlayerData PlayerRed 96 0

gameConfiguration :: GameConfiguration
gameConfiguration = GameConfiguration PlayerRed PlayerRed

