{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import Control.Applicative
import Control.DeepSeq
import Control.Exception
import Control.Lens
import Control.Monad
import Data.Foldable (toList)
import Data.Maybe
import Safe
import System.Environment

import Game.Board
import Game.Game
import Game.Logic
import Game.Replay
import Strategy.AlphaBeta

import Snapshots

import qualified Game.Situation as Situation

benchDepth :: Int -> Board -> GameConfiguration -> GameState -> IO SixpackTree
benchDepth 0 b _ gs = return $ Leaf None $ Situation.GameSituation b gs
benchDepth !n b gc gs = do
  tree <- benchDepth (pred n) b gc gs
  let (tree', statistic) = runLogger $ alphaBeta (calculateScore gc) $ tree >>= extendSituation
  let (_, statistic') = runLogger $ alphaBeta (calculateScore gc) tree'
  void $ evaluate $ force $ principalVariation tree'
  case tree' of
    Leaf _ _ -> return ()
    Fork _ cs -> print $ fst $ head $ toList cs
  putStrLn $ showStatistic statistic
  putStrLn $ showStatistic statistic'
  return tree'

main :: IO ()
main = do
  n <- fromMaybe 4 . (>>= readMay) . listToMaybe <$> getArgs
  forM_ snapshots $ \(name, (start, s@GameSnapshot{..})) -> do
    putStrLn $ ":: SNAPSHOT :: " ++ name
    void $ benchDepth n (board s) (GameConfiguration (gameState^.nextColor) start) gameState
    putStrLn ""
