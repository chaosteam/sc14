{-# LANGUAGE CPP #-}
module Main where

#ifdef USE_PROGRESSION
import Progression.Main
#else
import Criterion.Main
#endif

import Snapshots

import qualified AlphaBetaBenchmarks
import qualified BoardBenchmarks
import qualified MostPointsBenchmarks

main :: IO ()
main =
  defaultMain
    [ AlphaBetaBenchmarks.benchmarks snapshots
    , BoardBenchmarks.benchmarks
    , MostPointsBenchmarks.benchmarks snapshots
    ]
