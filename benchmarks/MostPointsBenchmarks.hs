{-# LANGUAGE RecordWildCards #-}
module MostPointsBenchmarks
  ( benchmarks
  ) where

import Control.Lens
import Criterion

import Game.Board
import Game.Game
import Game.Player
import Game.Replay
import Strategy.MostPoints
import Strategy.Strategy

import qualified BenchmarkData

benchSnapshot :: String -> (PlayerColor, GameSnapshot) -> Benchmark
benchSnapshot n (start, snapshot@GameSnapshot{..}) = bench ("snapshot/" ++ n) $ nf (view _2 . last . moves . strategy configuration gameState) $ board snapshot
  where configuration = GameConfiguration (gameState^.nextColor) start

benchmarks :: [(String, (PlayerColor, GameSnapshot))] -> Benchmark
benchmarks snapshots = bgroup "most-points" $
    bench "start" (nf (view _2 . last . moves . strategy BenchmarkData.gameConfiguration BenchmarkData.startGameState) newBoard)
  : map (uncurry benchSnapshot) snapshots
