{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE BangPatterns #-}
module AlphaBetaBenchmarks
  ( benchmarks
  ) where

import Control.Lens
import Criterion.Main

import Game.Board
import Game.Game
import Game.Logic
import Game.Player
import Game.Replay
import Strategy.AlphaBeta

import qualified BenchmarkData
import qualified Game.Situation as Situation

sortDepth :: Board -> Int -> GameConfiguration -> GameState -> SixpackTree
sortDepth b 0 _ gs = Leaf None $ Situation.GameSituation b gs
sortDepth b !n gc gs = evaluateLogger $ alphaBeta (calculateScore gc) $ sortDepth b (pred n) gc gs >>= extendSituation

benchSnapshot :: String -> (PlayerColor, GameSnapshot) -> Benchmark
benchSnapshot n (start, snapshot@GameSnapshot{..}) = bgroup ("snapshot/" ++ n)
  [ bench "deep1" $ nf (principalVariation . sortDepth (board snapshot) 1 configuration) gameState
  , bench "deep2" $ nf (principalVariation . sortDepth (board snapshot) 2 configuration) gameState
  , bench "deep3" $ nf (principalVariation . sortDepth (board snapshot) 3 configuration) gameState
  ]
  where configuration = GameConfiguration (gameState^.nextColor) start

benchmarks :: [(String, (PlayerColor, GameSnapshot))] -> Benchmark
benchmarks snapshots = bgroup "alpha-beta" $
   bgroup "start"
     [ bench "deep1" $ nf (principalVariation . sortDepth newBoard 1 BenchmarkData.gameConfiguration) BenchmarkData.startGameState
     , bench "deep2" $ nf (principalVariation . sortDepth newBoard 2 BenchmarkData.gameConfiguration) BenchmarkData.startGameState
     ]
 : map (uncurry benchSnapshot) snapshots
