{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module Main (main) where

import Blaze.ByteString.Builder
import Control.Exception
import Control.Lens hiding (argument, strict)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Either
import Control.Monad.Trans.Resource
import Data.Conduit
import Data.Conduit.Network
import Data.Monoid
import Network.Socket (close, setSocketOption, SocketOption(NoDelay, Cork), withSocketsDo)
import Options.Applicative
import System.IO
import System.IO.Error
import Text.XML.Stream.Parse
import Text.XML.Stream.Render

import qualified Data.Conduit.List as CL
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Network.BSD as N
import qualified Network.Socket as N

import Client.Client
import Client.Error
import Client.Protocol
import Client.Run
import Game.Game
import Strategy.List

data ClientArgs = ClientArgs
  { host :: String
  , port :: Int
  , reservation :: Maybe T.Text
  , strategyName :: Maybe String
  , strict :: Bool
  }

clientArgs :: ParserInfo ClientArgs
clientArgs = info (helper <*> parser) $ fullDesc <> header "A bot playing the game `Sixpack` created for the SoftwareChallenge 2014."
  where parser = ClientArgs
               <$> strOption (value "localhost" <> short 'h' <> long "host" <> metavar "HOST" <> help "The host on which the game server runs.")
               <*> option (value 13050 <> short 'p' <> long "port" <> metavar "PORT" <> help "The port of the game server to connect to.")
               <*> optional (fmap T.pack $ strOption $ short 'r' <> long "reservation" <> metavar "CODE" <> help "Use a reservation code to join a specific game.")
               <*> optional (strOption (short 's' <> long "strategy" <> metavar "STRATEGY" <> help "Use the given strategy"))
               <*> switch (long "strict" <> help "Enable strict mode. In strict mode, the client does not try to correct errors and never resets the strategy.")
               <*  optional (argument return mempty)  -- Hack: The game server gui passes "" as a last argument when executing, for whatever reason

-- Returns the first action from a list which does not throw an exception.
-- If all the actions throw exceptions (and the list of actions is not empty),
-- the last exception is thrown.
firstSuccessful :: [IO a] -> IO a
firstSuccessful [] = error "firstSuccessful: empty list"
firstSuccessful (p:ps) = catch p $ \(e :: IOException) ->
    case ps of
        [] -> throwIO e
        _  -> firstSuccessful ps

connect :: String -> Int -> IO N.Socket
connect hostname portnr = do
  proto <- N.getProtocolNumber "tcp"
  let hints = N.defaultHints { N.addrFlags = [N.AI_ADDRCONFIG]
                             , N.addrProtocol = proto
                             , N.addrSocketType = N.Stream
                             }
  addrs <- N.getAddrInfo (Just hints) (Just hostname) (Just $ show portnr)
  firstSuccessful $ map tryToConnect addrs
  where
  tryToConnect addr =
    bracketOnError
      (N.socket (N.addrFamily addr) (N.addrSocketType addr) (N.addrProtocol addr))
      N.sClose  -- only done if there's an error
      (\sock -> sock <$ N.connect sock (N.addrAddress addr))

ignoreIOException :: IO () -> IO ()
ignoreIOException a = void (try a :: IO (Either IOException ()))

main :: IO ()
main = withSocketsDo $ do
  handleForked
  args <- execParser clientArgs
  strategy <- resolveStrategyName $ strategyName args
  putStrLn $ "Connecting to server " ++ host args ++ ":" ++ show (port args) ++ " ..."
  hSetBuffering stdout LineBuffering
  es <- try $ connect (host args) $ port args
  case es of
    Left e | isDoesNotExistError e -> putStrLn "Couldn't connect to the game server. Is the server running?"
    Left e -> throwIO e
    Right s ->  flip finally (close s) $ do
      ignoreIOException $ setSocketOption s Cork 0
      ignoreIOException $ setSocketOption s NoDelay 1
      runResourceT $ sourceSocket s
        $= parseBytes def
        $= (runEitherT (client strategy (reservation args) $ strict args) >>= liftIO . either (T.putStrLn . showError) printResult)
        $$ renderBuilder def
        =$ CL.map toByteString
        =$ sinkSocket s

  where printResult gameResult = case gameResult^.winner of
          Nothing -> T.putStrLn $ "Tie!\n" <> gameResult^.reason
          Just p  -> T.putStrLn $ "Player " <> showPlayerColor p <> " won!\n" <> gameResult^.reason
