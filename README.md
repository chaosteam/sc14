# softwarechallenge14 [![wercker status](https://app.wercker.com/status/5932cb5e73db97df5fb1c3982680f314/s "wercker status")](https://app.wercker.com/project/bykey/5932cb5e73db97df5fb1c3982680f314)

This is our client for the SoftwareChallenge 2014 competition, which won the first prize. The goal was to write a bot that plays the Sixpack game. More information about the game and the competition is available on the official website of the competition: http://www.informatik.uni-kiel.de/software-challenge/2014/.

The strategy is a simple alpha beta search, using the score difference to evaulate game positions.

Project structure
-----------------
+ `source`: This is where the main development happens. It contains the actual implementation of the client. There are 3 sub-parts:
    + `Game`: Everything that is used to implement the game logic and related game entities.
    + `Client`: Implementation of the protocol to communicate with the server. This is where all the IO takes place.
    + `Strategy`: Implementation of different strategies.
+ `contest`: This directory contains a tool to interface with the contest web site (upload new clients, download replays, delete old clients, etc.)
+ `benchmarks`: Benchmarks for the client (testing AlphaBeta speed, etc.)
+ `tests`: All tests for the client.
+ `data`: Auxillary data, for example replay files.
+ `scripts`: Scripts, including a script for building and uploading a new client.
+ `interactive`: Haskell source files for exploration in GHCi, also little haskell files that don't belong anywhere else (like reproductions of a bug).

Building
--------
The project uses cabal to build. If you just want to build the client without building tests, bechmarks or the contest tool, you can run the following series of commands at the root of the project:

    $ cabal sandbox init # Requires cabal-install version >= 1.18
	$ cabal install --only-dependencies -f-contest
	$ cabal configure -f-contest
	$ cabal build

The `-f-contest` argument disables the contest tool. If you wish to build it, you can just omit that argument.

You can also use `--enable-tests` or `--enable-benchmarks` to build the tests or benchmarks.

There are also two other cabal flags, which can be enabled with `-f<flag name>`:

+ `new-criterion`: Use a newer version of the `criterion` library used for benchmarking. You can only use this flag if you install a patched version of `criterion` from https://github.com/bos/criterion.
+ `progression`: Use the `progression` library to visualize benchmark results. This draws pretty graphics that make it easy to compare the relative difference in performance of different versions of the client. To use this, you need to install a patched version of `progression`. The patch can be found in `data/progression.patch`.

After building, you can run the client with:

    $ dist/build/client/client
