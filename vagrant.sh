sudo apt-get update
sudo apt-get install -y zip make libgmp3-dev

mkdir /install
cd /install

wget https://www.haskell.org/ghc/dist/7.6.3/ghc-7.6.3-i386-unknown-linux.tar.bz2
tar -xf ghc-7.6.3-i386-unknown-linux.tar.bz2 
cd ghc-7.6.3
./configure
sudo make install
cd ..

wget https://github.com/haskell/cabal/archive/cabal-install-v1.18.0.2.tar.gz
tar -xf cabal-install-v1.18.0.2.tar.gz 
sudo chmod 777 -R cabal-cabal-install-v1.18.0.2
cd cabal-cabal-install-v1.18.0.2/cabal-install
HOME=/home/vagrant sudo -u vagrant ./bootstrap.sh
HOME=/home/vagrant sudo -u vagrant /home/vagrant/.cabal/bin/cabal update
cd ..

rm -r /install

cat >> /home/vagrant/.profile << 'EOF'
export PATH=/home/vagrant/.cabal/bin:$PATH
EOF

cat >> /home/vagrant/.bashrc <<EOF
cd /vagrant
EOF
