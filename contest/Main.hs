module Main where

import Control.Exception
import Control.Lens hiding ((<.>), argument)
import Control.Monad
import Control.Monad.Trans
import Data.Monoid
import Options.Applicative hiding ((&))
import Safe
import System.Directory
import System.Exit
import System.FilePath
import Text.Regex.Applicative

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Foldable as F

import API
import Browser hiding (header)
import Session

import Build_contest

main :: IO ()
main = do
  creds <- readDefaultCredentials
  args <- execParser $ argsParser creds
  getSession (user args) (password args) >>= maybe loginFailed (runBrowser $ app $ mode args)
 where
  loginFailed = putStrLn "Login failed. Please check your login credentials." >> exitFailure

app :: Mode -> Browser ()
app Sync = sync
app Clients  = clients
app (Upload file name) = upload file name
app (Fetch url) = fetch url

sync :: Browser ()
sync = do
  team <- getDefaultTeam
  client <- headNote "Not a single client available" <$> getClients team
  matches <- clientMatches <$> getClientInfo client
  F.for_ matches $ getMatchInfo >=> downloadMatch team

downloadMatch :: Team -> MatchInfo -> Browser ()
downloadMatch self m = do
  liftIO $ putStrLn $ "Download | " ++ teamName enemy
  liftIO $ createDirectoryIfMissing True (downloadDirectory </> teamName enemy)
  F.for_ (matchRounds m) $ downloadRound (downloadDirectory </> teamName enemy)
  return ()
 where
   enemy :: Team
   enemy = case matchOpponents m of
    (a,b) | a /= self  -> a
          | otherwise -> b

downloadRound :: String -> RoundInfo -> Browser ()
downloadRound dir roundInfo = do
  alreadyDownloaded <- liftIO $ doesFileExist targetFile
  unless alreadyDownloaded $ do
    replay <- get (roundReplayUrl roundInfo)
    liftIO $ LBS.writeFile targetFile (replay^.responseBody) `onException` removeFile targetFile
    return ()
 where
  targetFile :: FilePath
  targetFile = dir </> show (roundId roundInfo) <.> "xml.gz"

clients :: Browser ()
clients = do
  team <- getDefaultTeam
  clientsInfo <- getClients team >>= traverse getClientInfo
  F.for_ (zip [0::Int ..] clientsInfo) $ \(n, clientInfo) ->
    liftIO $ putStrLn $ pad 2 ' ' (show n) ++ " | " ++ pad 4 ' ' (maybe "-" statusString $ clientTestStatus clientInfo) ++ " | " ++ clientName clientInfo
 where
  pad :: Int -> a -> [a] -> [a]
  pad n c ls = ls ++ replicate (n - length ls) c

  statusString :: TestStatus -> String
  statusString TestSuccess = "ok"
  statusString TestFailure = "fail"
  statusString InProgress  = "..."

upload :: FilePath -> String -> Browser ()
upload file name = do
  team <- getDefaultTeam
  archive <- liftIO $ LBS.readFile file
  client <- uploadClient team name archive
  setClientMain client "client"
  testClient client True

fetch :: String -> Browser ()
fetch url = do
  team <- getDefaultTeam
  url' <- case match (few anySym *> optional (string "contest.software-challenge.de") *> many anySym) url of
    Nothing -> liftIO $ putStrLn "Error: Invalid match url" >> exitFailure
    Just url' -> return url'
  matchInfo <- getMatchInfo $ Match url'
  downloadMatch team matchInfo

--------------------------------------------------------------------------------
credentialsFile :: String
credentialsFile = head getSrcDirs </> ".credentials"

readDefaultCredentials :: IO (Maybe String, Maybe String)
readDefaultCredentials = parse . lines <$> readFile credentialsFile where
  parse :: [String] -> (Maybe String, Maybe String)
  parse ls = (ls ^? ix 0, ls ^? ix 1)

data Args = Args
  { user :: String
  , password :: String
  , mode :: Mode
  }

data Mode = Sync
          | Clients
          | Upload FilePath String
          | Fetch String

downloadDirectory :: FilePath
downloadDirectory = head getSrcDirs </> "download"

syncCmd :: ParserInfo ()
syncCmd = void $ info helper $ fullDesc <> progDesc ("Sync all replays for the latest uploaded client. The replays will be stored in " ++ downloadDirectory)

clientsCmd :: ParserInfo ()
clientsCmd = void $ info helper $ fullDesc <> progDesc "Show all clients for your team."

uploadCmd :: ParserInfo Mode
uploadCmd = info (helper <*> parser) $ fullDesc <> progDesc "Upload a new client and set the main file. Also runs the tests and activates the client when the tests succeeded." where
  parser :: Parser Mode
  parser = Upload <$> fileArg <*> nameArg

  fileArg :: Parser FilePath
  fileArg = argument Just $ metavar "FILE"
    <> help "The file to upload. Should be a zip archive."

  nameArg :: Parser String
  nameArg = strOption $ short 'n' <> long "name" <> metavar "STRING" <> value ""
    <> help "The name for the client. Defaults to no name."

fetchCmd :: ParserInfo String
fetchCmd = info (helper <*> parser) $ fullDesc <> progDesc "Fetch a particular match. This match will be stored in the download directory, like all the synced matches." where
  parser :: Parser String
  parser = argument Just $ metavar "URL" <> help "URL of the match to fetch."

argsParser :: (Maybe String, Maybe String) -> ParserInfo Args
argsParser (defaultUser, defaultPassword) = info (helper <*> parser) $ header "CLI interface for the softwarechallenge contest system" where
  parser :: Parser Args
  parser = Args <$> userArg <*> passwordArg <*> subparser modes

  modes :: Mod CommandFields Mode
  modes = mconcat
    [ command "sync"     $ Sync     <$ syncCmd
    , command "clients"  $ Clients  <$ clientsCmd
    , command "upload"     uploadCmd
    , command "fetch"    $ Fetch   <$> fetchCmd
    ]

  userArg :: Parser String
  userArg = strOption $ mconcat
    [ short 'u' <> long "user" <> metavar "USER"
    , showDefault <> F.foldMap value defaultUser
    , help $ "The username of the account to use for logging in. The first line of " <> credentialsFile <> " is used as a default for this option."
    ]

  passwordArg :: Parser String
  passwordArg = strOption $ mconcat
    [ short 'p' <> long "password" <> metavar "PASS"
    , showDefault <> F.foldMap value defaultPassword
    , help $ "The password to use. The second line of " <> credentialsFile <> " is used as a default for this option."
    ]
