{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
module Session where

import Network.HTTP.Client (CookieJar)
import Network.Wreq
import Text.Regex.Applicative
import Text.XML.Lens

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text as T
import qualified Data.Text.Lens as T

import Util

-- | A session holds all the cookies needed to authenticate the user.
data Session = Session
  { authCookies :: CookieJar
  , authToken :: String
  }

-- | Given the source code of a page from the contest system, extract the authenticity_token. It returns Nothing if
-- the token could not be extract (for example, because the user isn't logged in).
extractAuthenticityToken :: LBS.ByteString -> Maybe String
extractAuthenticityToken = preview $ asXML . deep logoutOnclick . extractToken where
  logoutOnclick :: Traversal' Element T.Text
  logoutOnclick = el "a" . attributeSatisfies "href" isLogoutURL . attr "onclick"

  isLogoutURL :: T.Text -> Bool
  isLogoutURL = T.isPrefixOf "http://contest.software-challenge.de/logout"

  tokenRegex :: RE Char String
  tokenRegex = string "s.setAttribute('value', '" *> few anySym <* "');"

  extractToken :: Fold T.Text String
  extractToken = T.unpacked . folding (findFirstInfix tokenRegex) . _2

-- | Perform a login, using the given user/password pair and return a new session.
getSession :: String -> String -> IO (Maybe Session)
getSession user password = mkSession <$> post "http://contest.software-challenge.de/login" loginFormValues where
  loginFormValues :: [FormParam]
  loginFormValues =
    [ "user[email]" := user
    , "user[password]" := password
    , "commit" := "Anmelden"
    ]

  mkSession :: Response LBS.ByteString -> Maybe Session
  mkSession res = Session (res^.responseCookieJar) <$> extractAuthenticityToken (res^.responseBody)
