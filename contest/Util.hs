{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
module Util where

import Control.Lens
import Text.HTML.DOM (parseLBS)
import Text.Read
import Text.Regex.Applicative
import Text.XML.Lens

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text as T
import qualified Data.Text.Lens as T

-- | A Getter that parses an XML document and returns it's root.
asXML :: Getter LBS.ByteString Element
asXML = to $ documentRoot . parseLBS

-- | Fold links with a 'href' matching the given regex. Return the result
-- of the regex and the link text (stripped).
linkHrefRegex :: RE Char a -> Fold Element (a, String)
linkHrefRegex r = el "a" . runFold ((,) <$> Fold (attr "href" . T.unpacked . folding (match r)) <*> Fold (text . to T.strip . T.unpacked))

-- | Check that an element has the given class attribute.
hasClass :: T.Text -> Fold Element Element
hasClass cls = attributeSatisfies "class" (elem cls . T.words)

-- | A 'Prism' for that uses 'readMaybe' to parse a text value.
asRead :: (Read a, Show a) => Prism' T.Text a
asRead = T.unpacked . prism' show readMaybe
