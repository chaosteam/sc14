{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module AlphaBetaTests
  ( tests
  ) where

import Control.Applicative
import Control.Lens hiding (elements)
import Data.Function
import Data.List
import Data.List.NonEmpty (NonEmpty(..))
import Data.Ord
import Test.HUnit
import Test.QuickCheck
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.Tasty.TH

import qualified Data.List.NonEmpty as NE

import Game.Logic (Points, Score(..))
import Strategy.AlphaBeta

newtype SmallNum = SmallNum Int deriving (Eq, Ord, Num, Enum, Show, Read)

instance Bounded SmallNum
  where minBound = -3
        maxBound = 10

instance Arbitrary SmallNum where
  arbitrary = elements [minBound..maxBound]

instance Arbitrary a => Arbitrary (NonEmpty a) where
  arbitrary = (:|) <$> arbitrary <*> arbitrary

limitSize :: Int -> Gen a -> Gen a
limitSize x a = sized $ \s -> resize (min x s) a

instance (Eq m, Arbitrary l, Arbitrary m) => Arbitrary (GameTree m l) where
  arbitrary = limitSize 10 $ sized $ \s -> case s of
    0 -> frequency
      [ (2, Leaf None <$> resize 10 arbitrary)
      , (1, choice s)
      ]
    _ -> choice $ pred s

    where choice s' = fmap (Fork None . NE.fromList . nubBy ((==) `on` fst)) $ flip vectorOf (resize s' arbitrary) =<< growingElements [1,2,2,3,3,4]

  -- Could be improved
  shrink (Fork _ cs) = map snd (NE.toList cs)
  shrink x = [x]

score :: GameTree m Points -> Score
score (Leaf _ l)   = Score l
score (Fork _ t) = score $ snd $ NE.head t

-- | Implement minimax for comparision to alpha beta
minimax :: (l -> Score) -> GameTree m l -> GameTree m l
minimax f = snd . go True
  where go _      (Leaf _ l)  = (f l, Leaf None l)
        go !doMax (Fork _ cs) = makeResult $ NE.sortBy ((if doMax then flip else id) $ comparing $ fst . snd) $ NE.map (over _2 $ go $ not doMax) cs
        makeResult cs'@((_,(s,_)) :| _) = (s, Fork None $ NE.map (over _2 snd) cs')

-- | Check that the alpha beta algorithm produces the same final result as the minimax algorithm.
prop_minimax :: GameTree Int Points -> Bool
prop_minimax tree = principalVariation sortedTree == principalVariation sortedTree' && score sortedTree == score sortedTree'
  where sortedTree  = evaluateLogger $ alphaBeta Score tree
        sortedTree' = minimax   Score tree

-- | Check that the alpha beta sort doesn't discard any choices.
prop_same_data :: GameTree Int Points -> Bool
prop_same_data tree = go tree $ evaluateLogger $ alphaBeta Score tree
  where go (Leaf _ l)  (Leaf _ l')  = l == l'
        go (Fork _ cs) (Fork _ cs') = map fst csSorted == map fst cs'Sorted && and (zipWith go (map snd csSorted) (map snd cs'Sorted))
          where csSorted  = NE.toList $ NE.sortOn fst cs
                cs'Sorted = NE.toList $ NE.sortOn fst cs'
        go _ _ = False

case_wikipedia_example :: Assertion
case_wikipedia_example = do
  Score 6   @=? score sortedTree
  [2,1,1,1] @=? principalVariation sortedTree
  where sortedTree = evaluateLogger $ alphaBeta Score treeExample

case_example_2 :: Assertion
case_example_2 = do
  score sortedTree'              @=? score sortedTree
  principalVariation sortedTree' @=? principalVariation sortedTree
  where sortedTree  = evaluateLogger $ alphaBeta Score treeExample2
        sortedTree' = minimax Score treeExample2

-- | An example that failed once.
treeExample2 :: GameTree Int Points
treeExample2 = read "Fork None ((-1,Fork None ((0,Fork None ((1,Leaf None 0) :| [])) :| [(-1,Fork None ((-1,Leaf None (-1)) :| []))])) :| [])"

-- | Example taken from wikipedia
treeExample :: GameTree Int Points
treeExample = choice
  [ choice
    [ choice
      [ leafChoice [5,6]
      , leafChoice [7,4,5]
      ]
    , choice
      [ choice [leafChoice [3]]
      ]
    ]
  , choice
    [ choice
      [ leafChoice [6]
      , leafChoice [6,9]
      ]
    , choice
      [ leafChoice [7]
      ]
    ]
  , choice
    [ choice [leafChoice [5]]
    , choice
      [ leafChoice [9,8]
      , leafChoice [6]
      ]
    ]
  ]
  where choice :: [GameTree Int Points] -> GameTree Int Points
        choice = Fork None . NE.zip (1 :| [2..]) . NE.fromList
        leafChoice = choice . map return

tests :: TestTree
tests = $testGroupGenerator
