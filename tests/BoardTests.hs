{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module BoardTests where

import Control.Applicative
import Control.Lens hiding (elements)
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Data.List
import Data.Semigroup
import Test.QuickCheck.Gen
import Test.QuickCheck.Property
import Test.QuickCheck.Property.Monad
import Test.Tasty
import Test.Tasty.QuickCheck
import Test.Tasty.TH

import qualified Data.Map.Strict as M

import ArbitraryInstances()
import Game.Board
import Game.Position
import Game.Stone

import qualified Game.StoneSet as SS

startPosition :: Position
startPosition = Position 8 8

startStone :: Stone
startStone = head allStones

-- | For testing purposes, this data type implements a version
-- of the board where we store the stone at each field next to the real board.
data LoggedBoard = LoggedBoard
  { loggedBoard    :: M.Map Position Stone
  , realBoard      :: Board
  }

startBoard :: LoggedBoard
startBoard = insertStoneLogged startPosition startStone newLoggedBoard

showStone :: Stone -> String
showStone (Stone s c) = [showSymbol s, showColor c]

showSymbol :: StoneSymbol -> Char
showSymbol Acorn = 'A'
showSymbol Bell = 'B'
showSymbol Clubs = 'C'
showSymbol Diamond = 'D'
showSymbol Heart = 'H'
showSymbol Spades = 'S'

showColor :: StoneColor -> Char
showColor Blue = 'b'
showColor Green = 'g'
showColor Magenta = 'm'
showColor Orange = 'o'
showColor Violet = 'v'
showColor Yellow = 'y'

showPosition :: Position -> String
showPosition (Position x y) = "(" ++ show x ++ "|" ++ show y ++ ")"

prettyPrintBoard :: LoggedBoard -> String
prettyPrintBoard (LoggedBoard m _) = unlines (header : map showLine [0..15])
  where showLine y = unwords $ map (pad 2) $ show y : [ maybe "" showStone $ M.lookup (Position x y) m | x <- [0..15] ]
        header     = unwords $ "  " : map (pad 2 . show) [(0::Int)..15]
        pad n as   = replicate (n - length as) ' ' ++ as

newLoggedBoard :: LoggedBoard
newLoggedBoard = LoggedBoard M.empty newBoard

insertStoneLogged :: Position -> Stone -> LoggedBoard -> LoggedBoard
insertStoneLogged p s (LoggedBoard m b) = LoggedBoard (M.insert p s m) $ insertStone p s b

isValidSeries :: [Stone] -> Bool
isValidSeries xs = nub xs == xs && ty /= Invalid
  where ty = mconcat $ map (\s -> Ambiguous (s^.color) (s^.symbol)) xs

validStones :: Position -> M.Map Position Stone -> [Stone]
validStones pos m
  | M.member pos m = []
  | null vert && null hori && not (M.null m) = []
  | otherwise      = filter valid allStones
  where
    vert = go dUp pos ++ go dDown pos
    hori = go dRight pos ++ go dLeft pos
    go d !p = case M.lookup p' m of
      Nothing -> []
      Just st -> st : go d p'
      where p' = nextPosition d 1 p
    valid st = isValidSeries (st : vert) && isValidSeries (st : hori)

prop_board_valid :: Property
prop_board_valid = property $ (True <$) . runMaybeT . go newLoggedBoard =<< gen (sized return)
  where

    go :: LoggedBoard -> Int -> MaybeT PropM ()
    go _ 0 = return ()
    go !b !s = do
      let fs = allFreeFields $ realBoard b
      when (null fs) $ if checkEnd (loggedBoard b)
        then mzero
        else lift $ failWith $ "Invalid board: No free fields returned, but board still has free fields.\n" ++ prettyPrintBoard b

      (p,st) <- lift $ do
        (p,f) <- gen $ elements fs
        let sts = SS.toList $ allPossibleStones f

        assert ("Field at " ++ showPosition p ++ " should have possible stones.\n" ++ prettyPrintBoard b) $ not $ null sts
        assert ("Field at " ++ showPosition p ++ " should be free.\n" ++ prettyPrintBoard b) $ checkFree p $ loggedBoard b

        st <- gen $ elements sts
        assert ("Laying stone " ++ showStone st ++ " onto field " ++ showPosition p ++ " should be possible\n" ++ prettyPrintBoard b) $ checkPossible p st $ loggedBoard b
        (p,st) <$ logMessageLn ("Laying stone " ++ showStone st ++ " onto field " ++ showPosition p)

      go (insertStoneLogged p st b) (pred s)

    checkEnd :: M.Map Position Stone -> Bool
    checkEnd m = all (null . flip validStones m) [ Position x y | x <- [0..15], y <- [0..15]]

    checkFree :: Position -> M.Map Position Stone -> Bool
    checkFree = M.notMember

    checkPossible :: Position -> Stone -> M.Map Position Stone -> Bool
    checkPossible p st m = elem st $ validStones p m

tests :: TestTree
tests = $testGroupGenerator
