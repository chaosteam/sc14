module Main where

import Test.Tasty

import qualified AlphaBetaTests
import qualified BoardTests
import qualified LogicTests
import qualified StorableTests

main :: IO ()
main = defaultMain $ testGroup "softwarechallenge tests"
  [ BoardTests.tests
  , AlphaBetaTests.tests
  , LogicTests.tests
  , StorableTests.tests
  ]
