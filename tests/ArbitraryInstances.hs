{-# OPTIONS_GHC -fno-warn-orphans #-}
module ArbitraryInstances where

import Control.Applicative
import Data.Monoid
import Test.QuickCheck

import qualified Data.Map as M

import Game.Game
import Game.Player
import Game.Position
import Game.Stone

instance Arbitrary StoneSymbol where
  arbitrary = elements allStoneSymbols
  shrink _ = [ Acorn ]

instance Arbitrary StoneColor  where
  arbitrary = elements allStoneColors
  shrink _ = [ Green ]

instance Arbitrary PlayerColor where
  arbitrary = elements [PlayerRed, PlayerBlue]
  shrink _ = [ PlayerRed ]

instance Arbitrary Stone where
  arbitrary = liftA2 Stone arbitrary arbitrary
  shrink = liftA2 (<>) (color shrink) (symbol shrink)

instance Arbitrary Position where
  arbitrary = Position <$> elements [0..15] <*> elements [0..15]
  shrink _ = [ Position 15 15, Position 0 0, Position 3 5, Position 15 0, Position 0 15, Position 7 7]

instance Arbitrary Move where
  arbitrary = oneof
    [ ExchangeMove <$> (flip vectorOf arbitrary =<< elements [1..6])
    , LayMove <$> (flip vectorOf arbitrary =<< elements [1..6])
    ]
  shrink (ExchangeMove xs) = ExchangeMove <$> shrink xs
  shrink (LayMove xs) = LayMove <$> shrink xs

instance Arbitrary StoneCollection where
  arbitrary = makeStoneCollection . M.fromListWith (+) . flip zip (repeat 1) <$> (flip vectorOf arbitrary =<< elements [1..6])

instance Arbitrary GameState where
  arbitrary = GameState
              <$> (flip vectorOf arbitrary =<< elements [1..20])
              <*> arbitrary
              <*> arbitrary
              <*> elements [0..100]
              <*> elements [0..40]

instance Arbitrary s => Arbitrary (PlayerData s) where
  arbitrary = liftA2 PlayerData arbitrary arbitrary

instance Arbitrary PlayerState where
  arbitrary = liftA2 PlayerState (elements [0..1000]) arbitrary

instance Arbitrary MaybeMove where
  arbitrary = MaybeMove <$> arbitrary
