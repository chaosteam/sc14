{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}
module StorableTests
  ( tests
  ) where

import Foreign.Marshal.Alloc
import Foreign.Storable
import Test.QuickCheck
import Test.Tasty
import Test.Tasty.QuickCheck
import Test.Tasty.TH

import ArbitraryInstances()
import Game.Game
import Game.Player
import Game.Position
import Game.Stone
import StorableInstances()

testStorable :: (Eq a, Storable a) => a -> Property
testStorable x = ioProperty $ fmap (== x) $ alloca $ \ptr -> poke ptr x >> peek ptr

prop_storable_position :: Position -> Property
prop_storable_position = testStorable

prop_storable_stone :: Stone -> Property
prop_storable_stone = testStorable

prop_storable_stone_collection :: StoneCollection -> Property
prop_storable_stone_collection = testStorable

prop_storable_move :: Move -> Property
prop_storable_move = testStorable

prop_storable_maybe_move :: MaybeMove -> Property
prop_storable_maybe_move = testStorable

prop_storable_player_state :: PlayerState -> Property
prop_storable_player_state = testStorable

prop_storable_player_data :: PlayerData PlayerState -> Property
prop_storable_player_data = testStorable

prop_storable_game_state :: GameState -> Property
prop_storable_game_state = testStorable

tests :: TestTree
tests = $testGroupGenerator
