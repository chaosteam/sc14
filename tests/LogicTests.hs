{-# LANGUAGE TemplateHaskell #-}

module LogicTests
  ( tests
  ) where

import Control.Applicative
import Control.Lens
import Data.List
import Data.Maybe
import Data.Monoid
import Data.Ord
import Test.HUnit
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.TH

import qualified Data.Map as M

import Game.Board
import Game.Game
import Game.Logic
import Game.Player
import Game.Position
import Game.Situation
import Game.Stone

import qualified Game.StoneSet as SS

case_example_1 :: Assertion
case_example_1 = do
  pts      @?= 14
  bestMove @?= LayMove [(Stone Acorn Magenta, Position 8 8)]
  assertBool "Doesn't match the simple algorithmn" $ sort (nub $ allPossibleLayMoves b [] blueStones) == sort (nub $ allPossibleLayMovesSimple b [] blueStones)
  assertBool "Duplicate move" $ nub (allPossibleLayMoves b [] blueStones) == allPossibleLayMoves b [] blueStones
  where blueStones = makeStoneCollection $ M.fromList $ zip ?? repeat 1 $
          [ Stone Acorn   Magenta
          , Stone Clubs   Yellow
          , Stone Diamond Green
          , Stone Spades  Violet
          , Stone Clubs   Orange
          , Stone Diamond Yellow
          ]
        m1 = LayMove
          [ (Stone Clubs   Magenta, Position 3 8)
          , (Stone Bell    Magenta, Position 4 8)
          , (Stone Spades  Magenta, Position 5 8)
          , (Stone Diamond Magenta, Position 6 8)
          , (Stone Heart   Magenta, Position 7 8)
          ]
        m2 = LayMove
          [ (Stone Diamond Yellow, Position 6 9)
          , (Stone Heart   Yellow, Position 7 9)
          , (Stone Acorn   Yellow, Position 8 9)
          ]
        b = applyMove m1 $ applyMove m2 newBoard
        (pts, bestMove, _, _) = maximumBy (comparing $ view _1) $ allPossibleLayMoves b [] blueStones

case_example_2 :: Assertion
case_example_2 = do
  pts @?= 5
  assertBool "Doesn't match the simple algorithmn" $ sort (nub $ allPossibleLayMoves b [] blueStones) == sort (nub $ allPossibleLayMovesSimple b [] blueStones)
  assertBool "Duplicate move" $ nub (allPossibleLayMoves b [] blueStones) == allPossibleLayMoves b [] blueStones
  where blueStones = makeStoneCollection $ M.fromList $ zip ?? repeat 1 $
          [ Stone Spades  Violet
          , Stone Spades  Orange
          , Stone Bell    Orange
          , Stone Heart   Green
          , Stone Clubs   Magenta
          , Stone Diamond Orange
          ]
        m1 = LayMove
          [ (Stone Bell  Blue, Position 4 10)
          , (Stone Acorn Blue, Position 5 10)
          ]
        b = applyMove m1 newBoard
        (pts, _, _, _) = maximumBy (comparing $ view _1) $ allPossibleLayMoves b [] blueStones

case_example_3 :: Assertion
case_example_3 = do
  Just (5, m2) @=? fmap tuple2 (find ((== m2) . view _2) $ allPossibleLayMoves b [] redStones)
  assertBool "Doesn't match the simple algorithmn" $ sort (nub $ allPossibleLayMoves b [] redStones) == sort (nub $ allPossibleLayMovesSimple b [] redStones)
  assertBool "Duplicate move" $ nub (allPossibleLayMoves b [] redStones) == allPossibleLayMoves b [] redStones
 where
  redStones :: StoneCollection
  redStones = makeStoneCollection $ M.fromList $ zip ?? repeat 1 $
    [ Stone Acorn   Violet
    , Stone Diamond Green
    , Stone Bell    Yellow
    , Stone Bell    Orange
    , Stone Bell    Blue
    , Stone Heart   Blue
    ]

  m :: Move
  m = LayMove
    [ (Stone Acorn  Violet, Position 5 0)
    , (Stone Spades Violet, Position 6 0)
    , (Stone Clubs  Violet, Position 6 1)
    , (Stone Clubs  Violet, Position 7 0)
    , (Stone Clubs  Green,  Position 7 1)
    , (Stone Clubs  Yellow, Position 7 2)
    , (Stone Clubs  Blue,   Position 7 3)
    , (Stone Bell   Violet, Position 8 0)
    , (Stone Heart  Violet, Position 9 0)
    , (Stone Heart  Yellow, Position 9 1)
    , (Stone Heart  Orange, Position 9 2)
    ]

  m2 :: Move
  m2 = LayMove
    [ (Stone Bell Yellow, Position 6 4)
    , (Stone Bell Orange, Position 6 5)
    , (Stone Bell Blue,   Position 6 3)
    ]

  b :: Board
  b = applyMove m newBoard

case_orthogonal_bug :: Assertion
case_orthogonal_bug = Just (14, m) @=? fmap tuple2 (listToMaybe $ sortBy (flip $ comparing $ view _1) $ allPossibleLayMoves b [] redStones) where
  redStones :: StoneCollection
  redStones = makeStoneCollection $ M.fromListWith (+) $ zip ?? repeat 1 $
    [ Stone Acorn Blue
    , Stone Acorn Violet
    ] ++ replicate 4 (Stone Spades Yellow)

  b :: Board
  b = applyMove ?? newBoard $ LayMove
    [ (Stone Clubs   Violet, Position 0 0)
    , (Stone Spades  Violet, Position 0 1)
    , (Stone Diamond Violet, Position 0 2)
    , (Stone Bell    Violet, Position 0 3)
    , (Stone Heart   Violet, Position 0 4)
    ]

  m :: Move
  m = LayMove
    [ (Stone Acorn   Violet, Position 0 5)
    , (Stone Acorn   Blue  , Position 1 5)
    ]

case_score_bug :: Assertion
case_score_bug = Just (2, m) @=? fmap tuple2 (listToMaybe $ sortBy (flip $ comparing $ view _1) $ allPossibleLayMoves b [] redStones) where
  redStones :: StoneCollection
  redStones = makeStoneCollection $ M.fromListWith (+) $ zip ?? repeat 1 $
    Stone Acorn Blue : replicate 5 (Stone Spades Yellow)

  b :: Board
  b = applyMove ?? newBoard $ LayMove
    [ (Stone Acorn Yellow, Position 0 0)
    , (Stone Acorn Blue  , Position 0 1)
    ]

  m :: Move
  m = LayMove [ (Stone Acorn Blue, Position 1 0) ]

case_valid_start_move :: Assertion
case_valid_start_move = mapM_ (check . fst) $ possibleNextSituations situation where
  situation :: GameSituation
  situation = GameSituation newBoard gamestate

  gamestate :: GameState
  gamestate = GameState [] ps PlayerRed 0 10

  ps :: PlayerData PlayerState
  ps = PlayerData (PlayerState 10 redStones) (PlayerState 10 blueStones)

  redStones :: StoneCollection
  redStones = makeStoneCollection $ M.fromListWith (+) $ zip ?? repeat 1 $
       replicate 4 (Stone Acorn Magenta)
    ++ replicate 2 (Stone Acorn Blue)

  blueStones = makeStoneCollection $ M.fromListWith (+) $ zip ?? repeat 1 $
    replicate 6 (Stone Bell Yellow)

  check :: Move -> Assertion
  check m = assertBool (show m ++ " is not a valid start move") $ isValidStartMove m

tuple2 :: (a, b, c, d) -> (a, b)
tuple2 (x,y,_,_) = (x,y)

--------------------------------------------------------------------------------
allPossibleLayMovesSimple :: Board -> [Stone] -> StoneCollection -> [(Points, Move, Board, ([Stone], StoneCollection))]
allPossibleLayMovesSimple b ns stc = do
  axis <- [XAxis, YAxis]
  (p, field) <- allFreeFields b
  buildMovesSimple axis field p b (ns, stc) (SS.fromList $ stoneList stc)

buildMovesSimple :: Axis -> Field -> Position -> Board -> ([Stone], StoneCollection) -> SS.StoneSet -> [(Points, Move, Board, ([Stone], StoneCollection))]
buildMovesSimple axis f = build False mempty (Just f)
  where build :: Bool -> PartialMove -> Maybe Field -> Position -> Board -> ([Stone], StoneCollection) -> SS.StoneSet -> [(Points, Move, Board, ([Stone], StoneCollection))]
        build stop partialMove maybeField position b stonebuffer hand = do
          field <- maybeToList maybeField
          stone <- SS.toList $ SS.intersection hand $ allPossibleStones field
          let partialMove' = partialMove <> layStone stone position axis field
              (ps, move)   = finalizeMove partialMove' $ 1 + getHoledSeriesLength field axis
              board'       = insertStone position stone b
              hand'        = SS.delete stone hand
              stonebuffer' = takeStone stone stonebuffer
          (ps, move, board', stonebuffer') : do
            direction <- Direction axis <$> True : if stop then [] else [False]
            let position' = nextPosition direction (getSeriesLength field direction + 1) position
                maybeField' = lookupPosition position' board'
                stop'       = stop || isForwards direction
            build stop' partialMove' maybeField' position' board' stonebuffer' hand'

tests :: TestTree
tests = $testGroupGenerator
