{-# OPTIONS_GHC -fno-warn-unused-imports #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RankNTypes #-}
-- Useful functions for working with replay files
module Replay where

import Control.Applicative
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Either
import Control.Monad.Trans.Resource
import Data.Conduit
import Data.Conduit.Binary (sourceFile)
import Data.Conduit.Zlib
import Data.Maybe
import Data.Monoid
import Data.Void
import System.Exit
import Text.XML.Stream.Parse

import qualified Data.Map as M
import qualified Data.Text as T

import Client.Error
import Client.LaxXML

-- Imports for interactive use
import Client.Protocol
import Game.Board
import Game.Game
import Game.Logic
import Game.Player
import Game.Position
import Game.Situation
import Game.Stone
import Strategy.AlphaBeta
import Strategy.Strategy

withReplayFile :: String -> Parser Void (ResourceT IO) a -> IO a
withReplayFile f p = runResourceT $ sourceFile f $= ungzip $= parseBytes def $$ runEitherT p >>= fmap (either absurd id) . _Left
    (\err -> liftIO $ putStrLn (T.unpack $ showError err) >> exitFailure
    )

foldTurns :: Monoid a => (GameConfiguration -> Move -> GameState -> GameState -> Board -> Board -> a) -> Parser' a
foldTurns f = do
  situations <- map fst <$> forM [0..40] parseSituation
  return $ mconcat $ zipWith g situations $ drop 1 situations

  where g before after = f (gameConfiguration before) (fromJust $ lastMove after) (gameState before) (gameState after) (board before) (board after)

movePoints :: GameState -> GameState -> Integer
movePoints before after = truncate $ after^.currentPlayerData.points - before^.nextPlayerData.points

newtype Frequency a = Frequency (M.Map a Integer)
instance Ord a => Monoid (Frequency a) where
  mempty = Frequency mempty
  mappend (Frequency a) (Frequency b) = Frequency $ M.unionWith (+) a b

freqSingle :: a -> Frequency a
freqSingle = Frequency . flip M.singleton 1

freqShow :: (Ord a, Show a) => Frequency a -> IO ()
freqShow (Frequency m) = putStrLn $ unwords $ map entry $ M.toList m
  where entry (a,b) = show a ++ "[" ++ show b ++ "]"

guardM :: Monoid a => Bool -> a -> a
guardM False = const mempty
guardM True  = id

isLayMove :: Move -> Bool
isLayMove (LayMove _) = True
isLayMove _           = False
