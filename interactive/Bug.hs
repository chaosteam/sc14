module Main where

import Control.Applicative
import Control.Lens
import Control.Monad
import Data.Maybe
import Data.Monoid

import Client.Error
import Client.Run
import Game.Game
import Game.Player
import Game.Stone
import Strategy.Strategy

import qualified Game.Replay as Replay
import qualified Strategy.AlphaBeta as AlphaBeta

gameConfiguration :: GameConfiguration
gameConfiguration = GameConfiguration PlayerRed PlayerRed

newtype EndoKleisli m a = EndoKleisli (a -> m a)
instance Monad m => Monoid (EndoKleisli m a) where
  mempty = EndoKleisli return
  mappend (EndoKleisli f) (EndoKleisli g) = EndoKleisli $ f >=> g

create :: Replay.Replay -> Strategy
create replay = AlphaBeta.strategy
   gameConfiguration
   (Replay.gameState . head . tail . Replay.gameSnapshots $ replay)
   (Replay.board . head . Replay.gameSnapshots $ replay)

run :: Strategy -> EndoKleisli IO Strategy -> IO Strategy
run strategy (EndoKleisli f) = f strategy

runReplay :: (GameState -> [Replay.GameSnapshot] -> EndoKleisli IO Strategy) -> Replay.Replay -> IO Strategy
runReplay action = run <$> create <*> liftA2 action (fst . Replay.gameResult) (tail . Replay.gameSnapshots)

actSelf :: Show a => (Strategy -> [(String, a, Strategy)]) -> Int -> EndoKleisli IO Strategy
actSelf f n = EndoKleisli $ \strategy -> case f strategy !! n of
  (hint, move, strategy') -> seq move $ strategy' <$ do
    putStrLn $ "[STATISTIC] " ++ hint
    putStrLn $ "SELF :: " ++ show move

moveEnemy :: Move -> GameState -> EndoKleisli IO Strategy
moveEnemy move gameState = EndoKleisli $ \strategy -> do
  putStrLn $ "ENEMY :: " ++ show move
  return $ handleEnemy strategy gameState move

ownActions :: [EndoKleisli IO Strategy]
ownActions = map (actSelf moves) [4,2,2,1,3,1,2]
             ++ [ actSelf moves 1 <> actSelf improvements 1
                , actSelf moves 0 <> actSelf improvements 1] ++
             map (actSelf moves) [1,2,3,1,2]

actions :: GameState -> [Replay.GameSnapshot] -> EndoKleisli IO Strategy
actions gameStateFinish snapshots = mconcat . zipWith (<>) ownActions . map enemyAction $ [1,3 .. 39] where
  enemyAction n = moveEnemy (Replay.move $ snapshots !! n) $ fromMaybe gameStateFinish $ snapshots ^? ix (n + 1) . to Replay.gameState

main :: IO ()
main = do
  replay <- exitOnError $ loadReplay "data/lay_move_bug.xml"
  void $ runReplay actions replay { Replay.gameSnapshots = drop 3 $ Replay.gameSnapshots replay }
